/*
 * Funcoes.c
 *
 *  Created on: Oct 13, 2021
 *      Author: Marcel Giraldi
 */

#include "main.h"
#include "usb_device.h"

#include <stdarg.h> //for va_list var arg functions
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "time.h"
#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_conf.h"
#include "stm32l4xx_hal_spi.h"
#include "usbd_cdc_if.h"

#include "Globais.h"
#include "spirit1.h"
#include "spirit1_IRQ.h"
#include "iis3dwb_reg.h"
#include "Funcoes.h"

void calcula_bateria (void)
{
	//=================================================
	//AD_Bateria = valor do ADC
	//v_bateria = milivolts da bateria
	//aux_bateria = porcentual da bateria
	//=================================================
	if (AD_Bateria >= 2103)
	{
		v_bateria = ((1.6179*AD_Bateria) + 99.412);
	}
	else
	{
		v_bateria = ((5.3333*AD_Bateria) - 7716);
	}
	P_bateria = v_bateria;
}
void calcula_temperatura (void)
{
	float aux_res, temp_lida, aux_temp;
	float coef_ang, coef_lin;

	v_NTC100k = (((float)(AD_NTC100k+60))*TENSAO_REG/4096.0);
	aux_res = (100000.0*(TENSAO_REG - v_NTC100k)/v_NTC100k);
	if (aux_res >= Res_NTC100k[0])
	{
		temp_lida = -40.0;
	}
	else
	{
		if (aux_res <= Res_NTC100k[38])
		{
			temp_lida = 150.0;
		}
		else
		{
			for (I = 1; I < 37; I++)
			{
				if ((aux_res <= Res_NTC100k[I]) && (aux_res > Res_NTC100k[I+1]))
				{
					break;
				}
			}
			coef_ang = (-5.0)/(Res_NTC100k[I] - Res_NTC100k[I+1]);
			aux_temp = (float)(-40.0 + (5.0*I));
			coef_lin = (aux_temp - (coef_ang*Res_NTC100k[I]));
			temp_lida = coef_lin + coef_ang*aux_res;
		}
	}
	T_NTC100k = (temp_lida*10);
}
void calcula_Vbat (void)
{
	// Aqui vamos ter que usar a tensão v_bateria e subtrair 200mV do 3V3 quando a bateria estiver abaixo de 3V5

	if (TENSAO_REG > 3300.0)
	{
		v_Vbat = TENSAO_REG;
	}
	else
	{
		v_Vbat = v_bateria - 50;
	}
}
void Prepara_Envio_Normal (void)
{
	num_pacote = 1;
	bytes_pacote = 0;
	total_ACC = 0.0;
	for (I = 0; I < NUM_DE_COLETAS; I++)
	{
		total_ACC += data_acceleration[I];
		tx_buffer[num_pacote][bytes_pacote] = (data_acceleration[I] & 0xFF);
		tx_buffer[num_pacote][bytes_pacote+1] = (data_acceleration[I] >> 8);
		bytes_pacote += 2;
		if (bytes_pacote >= QTD_BYTES_RF)
		{
			bytes_pacote = 0;
			num_pacote++;
		}
		if (num_pacote > NUM_DE_PACOTES)
		{
			asm("NOP");
			break;
		}
	}
}
void Preparar_Pacotes_full (void)
{
	int16_t aux_data_ACC;

	//==============================================================================
	// Preparar
	num_pacote = 1;
	bytes_pacote = 0;
	total_ACC = 0.0;
	for (I = 0; I < (NUM_DE_COLETAS/2); I++)
	{ // Tem que ser (NUM_DE_COLETAS/2)
		//================================================================================
		total_ACC += data_acceleration[I];
		tx_buffer[num_pacote][bytes_pacote] = (data_acceleration[I] & 0xFF);
		tx_buffer[num_pacote][bytes_pacote+1] = (data_acceleration[I] >> 8);
		bytes_pacote += 2;
		if (bytes_pacote >= QTD_BYTES_RF)
		{
			bytes_pacote = 0;
			num_pacote++;
		}
		//================================================================================
		if (I < ((NUM_DE_COLETAS/2)-1))
		{
			if (data_acceleration[I] > data_acceleration[I+1])
			{// Atual é maior que o próximo
				aux_data_ACC = (data_acceleration[I+1] + ((data_acceleration[I]-data_acceleration[I+1])/2));
			}
			else
			{// Atual é menor que o próximo
				aux_data_ACC = (data_acceleration[I] + ((data_acceleration[I+1]-data_acceleration[I])/2));
			}
			//================================================================================
			total_ACC += aux_data_ACC;
			tx_buffer[num_pacote][bytes_pacote] = (aux_data_ACC & 0xFF);
			tx_buffer[num_pacote][bytes_pacote+1] = (aux_data_ACC >> 8);
			bytes_pacote += 2;
			if (bytes_pacote >= QTD_BYTES_RF)
			{
				bytes_pacote = 0;
				num_pacote++;
			}
			//================================================================================
		}
		if (num_pacote > NUM_DE_PACOTES)
		{
			asm("NOP");
			break;
		}
	}
}
void Preparar_Envio_Duplo (void)
{
	//==============================================================================
	// Preparar
	num_pacote = 1; // Tem que ser 1 o indice 0 é do cabeçalho
	num_pacote2 = NUM_DE_PACOTES/2;
	bytes_pacote = 0;
	total_ACC = 0.0; //Zera para fazer média
	for (I = 0; I < (NUM_DE_COLETAS/2); I++)
	{ // Tem que ser (NUM_DE_COLETAS/2)
		//================================================================================
		total_ACC += data_acceleration[I]; // soma acelerações
		tx_buffer[num_pacote][bytes_pacote] = (data_acceleration[I] & 0xFF);
		tx_buffer[num_pacote][bytes_pacote+1] = (data_acceleration[I] >> 8);
		tx_buffer[num_pacote+num_pacote2][bytes_pacote] = (data_acceleration[I] & 0xFF);
		tx_buffer[num_pacote+num_pacote2][bytes_pacote+1] = (data_acceleration[I] >> 8);
		bytes_pacote += 2;
		if (bytes_pacote >= QTD_BYTES_RF)
		{
			bytes_pacote = 0;
			num_pacote++;
		}
		//================================================================================
		if (num_pacote > (NUM_DE_PACOTES/2))
		{
			asm("NOP");
			break;
		}
	}
	asm("NOP");
}
void delay_ms (uint16_t delay_ms)
{
	time_daley_ms = delay_ms;
	while (time_daley_ms)
	{

	}
}
#ifdef TESTE_IMPRIMIR_NA_USB
void Transf_Pontos (void)
{
	float V_aux1;
	int16_t gravidade;
	int16_t aux_int;

	IT1 = 16;
	// valor mínimo
	aux_int = (int16_t)rx_buffer[0][IT1+1];
	gravidade = (aux_int * 256) + (int16_t)rx_buffer[0][IT1];
	V_aux1 = (((float)(gravidade))*Escala_aux + Escala_const);
	V_minimo = V_aux1;
	// valor médio
	aux_int = (int16_t)rx_buffer[0][IT1+3];
	gravidade = (aux_int * 256) + (int16_t)rx_buffer[0][IT1+2];
	V_aux1 = (((float)(gravidade))*Escala_aux + Escala_const);
	Gravidade_media = V_aux1;
	// valor máximo
	aux_int = (int16_t)rx_buffer[0][IT1+5];
	gravidade = (aux_int * 256) + (int16_t)rx_buffer[0][IT1+4];
	V_aux1 = (((float)(gravidade))*Escala_aux + Escala_const);
	V_maximo = V_aux1;

	num_pacote = 1;
	J = 0;
	IT1 = 0;
	for (I = 0; I < NUM_DE_COLETAS; I++)
	{
		// No buffer pacotes_tx registra 1 se o pacote veio e 0 se não veio
		// Neste caso o indice começa do 1 pois o pacote 1 é o cabeçalho
		//=================================================================
		// No Buffer rx_buffer o indice 1 é o início dos pacotes
		// o indice 0 é o cabeçalho
		aux_int = (int16_t)rx_buffer[num_pacote][IT1+1];
		gravidade = (aux_int * 256) + (int16_t)rx_buffer[num_pacote][IT1];
		data_raw_acceleration[J] = gravidade;
		data_acceleration[J] = gravidade;
		J++;
		IT1 += 2;
		if (IT1 >= QTD_BYTES_RF)
		{
			num_pacote++;
			IT1 = 0;
		}
	}
}
#endif
