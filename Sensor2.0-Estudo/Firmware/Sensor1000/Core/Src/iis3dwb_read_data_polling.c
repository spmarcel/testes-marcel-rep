/* Includes ------------------------------------------------------------------*/
#include "main.h"

#include <math.h>
#include <string.h>
#include <stdio.h>
#include "iis3dwb_reg.h"
#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_conf.h"
#include "stm32l4xx_hal_spi.h"

#include "Globais.h"
#include "Funcoes.h"

/* Private macro -------------------------------------------------------------*/
#define BOOT_TIME 10 //ms
#define SENSOR_BUS SPI_ACC

/* Private variables ---------------------------------------------------------*/
static int16_t data_raw_temperature;
static uint8_t whoamI, rst;

/* Extern variables ----------------------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
static int32_t platform_write (void *handle, uint8_t reg, uint8_t *bufp, uint16_t len);
static int32_t platform_read (void *handle, uint8_t reg, uint8_t *bufp, uint16_t len);
static void platform_delay (uint32_t ms);

stmdev_ctx_t dev_ctx;

void iis3dwb_read_data_polling (void)
{
	uint8_t reg;
	uint32_t aux_id;
	uint16_t aux_dif1;
	uint16_t aux_I1;
	int16_t aux_data_ACC1;

	if (!inicializar_iis3dwb)
	{// Leitura do acelerômetro
		if (!leu_temp)
		{
			//===============================================================================================
			//***********************************************************************************************
			// Leitura da temperatura do acelerômetro
			//***********************************************************************************************
			iis3dwb_temp_flag_data_ready_get (&dev_ctx, &reg);
			if (reg)
			{
				// Read temperature data
				memset (&data_raw_temperature, 0x00, sizeof(int16_t));
				iis3dwb_temperature_raw_get (&dev_ctx, &data_raw_temperature);
				Valor_Temp = (int16_t)((iis3dwb_from_lsb_to_celsius (data_raw_temperature))*10);
				leu_temp = 1;
			}
			//***********************************************************************************************
			//===============================================================================================
		}
		else
		{
			tempo_total = 0;
			pointerToMemory = 0;
			if (qual_eixo > 2)
			{ // para não ocorrer overflow
				qual_eixo = 2;
			}
			time_desprezar = DESPREZAR_LEIT_ACC;
			while (time_desprezar)
			{
				iis3dwb_xl_flag_data_ready_get (&dev_ctx, &reg);
				if (reg)
				{
					iis3dwb_acceleration_raw_get (&dev_ctx, &accel);
				}
			}
			//===============================================================================================
			pointerToMemory = 0;
			time_out_Acc = ((TEMPO_CAPTURA*tickseg)/1000); // tempo de leitura do sensor 16G
			tempo_total = 0;
			while (time_out_Acc)
			{// leio o sensor de 16G
				iis3dwb_xl_flag_data_ready_get (&dev_ctx, &reg);
				if (reg)
				{
					// Ler acelerômetro, data_raw_acceleration retorna os valores dos 3 eixos
					iis3dwb_acceleration_raw_get (&dev_ctx, &accel);
					data_raw_acceleration[pointerToMemory] = accel;
					if (pointerToMemory < NUM_DE_COLETAS)
					{
						pointerToMemory++;
					}
					else
					{
						break;
					}
				}
			}
			tempo_decorrido = tempo_total*(1000/tickseg); // tempo decorrido para coletar NUM_DE_COLETAS
			//===============================================================================================
			// compara amplitudes para ver se não tem outlier e pega minimo e máximo valor
			menor_amplitude = 32760;	// +16G  32768-Máximo
			maior_amplitude = -32760;	// -16G -32768-Mínimo
			// pointerToMemory = número de leituras feitas, como começa do zero tem que ser I < pointerToMemory
			for (I = 0; I < pointerToMemory; I++)
			{
				if ((data_raw_acceleration[I] > 32760) || (data_raw_acceleration[I] < (-32760)))
				{// "É OutLier"
					if (I)
					{
						data_raw_acceleration[I] = data_raw_acceleration[I-1];
					}
					else
					{
						data_raw_acceleration[I] = 0;
					}
				}
				if (data_raw_acceleration[I] < menor_amplitude)
				{// Encontrar menor amplitude
					menor_amplitude = data_raw_acceleration[I];
				}
				if (data_raw_acceleration[I] > maior_amplitude)
				{// Encontrar maior amplitude
					maior_amplitude = data_raw_acceleration[I];
				}
			}
			//============================================================================
			// Complemento
			aux_dif1 = 0;
			aux_I1 = 0;
			case_tx = TIPO_ENVIO;
			switch (case_tx)
			{
			default:
			case TX_NORMAL:
				aux_dif1 = (NUM_DE_COLETAS - pointerToMemory);
				if (aux_dif1 < NUM_DE_COLETAS/4)
				{
					if (aux_dif1 > 0)
					{
						aux_I1 = (NUM_DE_COLETAS/aux_dif1);// Periodo
					}
				}
				break;
			case TX_DUPLO:
				aux_dif1 = ((NUM_DE_COLETAS/2) - pointerToMemory);
				if (aux_dif1 < NUM_DE_COLETAS/4)
				{
					if (aux_dif1 > 0)
					{
						aux_I1 = ((NUM_DE_COLETAS/2)/aux_dif1);// Periodo
					}
				}
				break;
			case TX_FULL:
				aux_dif1 = ((NUM_DE_COLETAS/2) - pointerToMemory);
				if (aux_dif1 < NUM_DE_COLETAS/4)
				{
					if (aux_dif1 > 0)
					{
						aux_I1 = ((NUM_DE_COLETAS/2)/aux_dif1);// Periodo
					}
				}
				break;
			}
			// Abaixo assumo a coleta "data_raw_acceleration" em "data_acceleration"
			step = aux_I1; // Delta
			I1 = 0;
			for (I = 0; I < NUM_MAX_TRANSM; I++)
			{
				data_acceleration[I] = data_raw_acceleration[I1];
				if ((aux_dif1) && (I >= step))
				{// caso preciso complememntar
					if (data_raw_acceleration[I1] > data_raw_acceleration[I1+1])
					{// Atual é maior que o próximo
						aux_data_ACC1 = (data_raw_acceleration[I1+1] + ((data_raw_acceleration[I1]-data_raw_acceleration[I1+1])/2));
						if (aux_data_ACC1 > data_raw_acceleration[I1]) aux_data_ACC1 = data_raw_acceleration[I1];
						if (aux_data_ACC1 < data_raw_acceleration[I1+1]) aux_data_ACC1 = data_raw_acceleration[I1+1];
					}
					else
					{// Atual é menor que o próximo
						aux_data_ACC1 = (data_raw_acceleration[I1] + ((data_raw_acceleration[I1+1]-data_raw_acceleration[I1])/2));
						if (aux_data_ACC1 > data_raw_acceleration[I1+1]) aux_data_ACC1 = data_raw_acceleration[I1+1];
						if (aux_data_ACC1 < data_raw_acceleration[I1]) aux_data_ACC1 = data_raw_acceleration[I1];
					}
					I++;
					data_acceleration[I] = aux_data_ACC1;
					step += aux_I1;
					aux_dif1--;
				}
				I1++;
			}
			//============================================================================
			// Prepara cabeçalho
			//tempo_decorrido = TEMPO_CAPTURA; // 1 segundo de leitura
			tx_buffer[0][0] = 0x1B;// Para identificar início do buffer
			tx_buffer[0][1] = 0x1B;// Para identificar início do buffer
			//==========================================================
			// abcd efgh ijkl mnop (2 bytes = 16 bits)
			// ab: ( 2 bits para qual eixo )
			// 00 => eixo X => += 0 (0x0000)
			// 01 => eixo Y => += 16384 (0x4000)
			// 10 => eixo Z => += 32768 (0x8000)
			//-----------------------------------
			// c: ( 1 bit para qual sensor )
			// 0 - acelerômetro 16G => += 0 (0x0000)
			// 1 - acelerômetro 50G => += 8192 (0x2000)
			//-----------------------------------
			// d efgh ijkl mnop: ( 13 bits para id-sensor )
			// Nesta configuração eu posso colocar até 2^13 sensores
			// número de sensor variando de 1 até 8191
			//==========================================================
			aux_id = 0;
			switch (qual_eixo)
			{// Identificação de qual eixo
			case 0:
				aux_id = 0;
				break;
			case 1:
				aux_id = 16384;
				break;
			case 2:
				aux_id = 32768;
				break;
			}
			// aqui é a leitura de 16G, então não uso o bit "c" porquê é 0, já está zerado no aux_id
			aux_id += 0;//id_sensor;
			tx_buffer[0][2] = aux_id & 0xFF;
			tx_buffer[0][3] = aux_id >> 8;
			tx_buffer[0][4] = tempo_decorrido & 0xFF;
			tx_buffer[0][5] = tempo_decorrido >> 8;
			// 6-7 % bateria
			// 8-9 Temperatura
			// 10-15 Data e Hora
			tx_buffer[0][16] = menor_amplitude & 0xFF;
			tx_buffer[0][17] = menor_amplitude >> 8;
			tx_buffer[0][20] = maior_amplitude & 0xFF;
			tx_buffer[0][21] = maior_amplitude >> 8;
			// coloca nos "pacotes" as coletas
			case_tx = TIPO_ENVIO;
			switch (case_tx)
			{
			default:
			case TX_NORMAL:
				Prepara_Envio_Normal ();
				total_ACC = (total_ACC/MATRIZ_AMOSTRA);
				break;
			case TX_DUPLO:
				Preparar_Envio_Duplo ();
				total_ACC = (total_ACC/(MATRIZ_AMOSTRA/2));
				break;
			case TX_FULL:
				Preparar_Pacotes_full ();
				total_ACC = (total_ACC/MATRIZ_AMOSTRA);
				break;
			}
			// média amplitude
			tx_buffer[0][18] = total_ACC & 0xFF;
			tx_buffer[0][19] = total_ACC >> 8;
			Estado_Leitura++;
		}
	}
	else
	{
		//=========================================================================================
		// Inicialização do acelerômetro
		dev_ctx.write_reg = platform_write;
		dev_ctx.read_reg = platform_read;
		dev_ctx.handle = &SENSOR_BUS;

		// Wait sensor boot time
		platform_delay (BOOT_TIME);
		// Check device ID
		iis3dwb_device_id_get (&dev_ctx, &whoamI);
		if (whoamI == IIS3DWB_ID)
		{
			// Restore default configuration
			iis3dwb_reset_set (&dev_ctx, PROPERTY_ENABLE);
			do
			{
				iis3dwb_reset_get (&dev_ctx, &rst);
			}
			while (rst);
			// Enable axis
			iis3dwb_xl_axis_selection_set (&dev_ctx, (qual_eixo+1));// 1=X, 2=Y, 3=Z, 0=XYZ
			// Enable Block Data Update
			iis3dwb_block_data_update_set (&dev_ctx, PROPERTY_ENABLE);
			// Set Output Data Rate
			iis3dwb_xl_data_rate_set (&dev_ctx, IIS3DWB_XL_ODR_26k7Hz);
			// Set full scale
			escala_g = IIS3DWB_16g;
			iis3dwb_xl_full_scale_set (&dev_ctx, escala_g);
			// Configure filtering chain(No aux interface)
			// Accelerometer - LPF1 + LPF2 path
			iis3dwb_xl_hp_path_on_out_set (&dev_ctx, IIS3DWB_LP_6k3Hz);
			iis3dwb_xl_filter_lp2_set (&dev_ctx, PROPERTY_ENABLE);
			if (inicializar_iis3dwb) inicializar_iis3dwb--;
		}
		tttc_conf_Acc++;
		//=========================================================================================
	}
}
static int32_t platform_write (void *handle, uint8_t reg, uint8_t *bufp, uint16_t len)
{
	HAL_GPIO_WritePin (IIS3_CS_GPIO_Port, IIS3_CS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit (&SPI_ACC, &reg, 1, 1000);
	HAL_SPI_Transmit (&SPI_ACC, bufp, len, 1000);
	HAL_GPIO_WritePin (IIS3_CS_GPIO_Port, IIS3_CS_Pin, GPIO_PIN_SET);
	return 0;
}
static int32_t platform_read (void *handle, uint8_t reg, uint8_t *bufp, uint16_t len)
{
	// Read command
	reg |= 0x80;
	HAL_GPIO_WritePin (IIS3_CS_GPIO_Port, IIS3_CS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit (&SPI_ACC, &reg, 1, 1000);
	HAL_SPI_Receive (&SPI_ACC, bufp, len, 1000);
	HAL_GPIO_WritePin (IIS3_CS_GPIO_Port, IIS3_CS_Pin, GPIO_PIN_SET);
	return 0;
}
static void platform_delay (uint32_t ms)
{
	delay_ms (ms);
}
