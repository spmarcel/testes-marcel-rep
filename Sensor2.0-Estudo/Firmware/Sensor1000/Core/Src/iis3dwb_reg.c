#include "iis3dwb_reg.h"
#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_spi.h"

#include "Globais.h"

int32_t iis3dwb_read_reg (stmdev_ctx_t* ctx, uint8_t reg, uint8_t* data, uint16_t len)
{
	int32_t ret;
	ret = ctx->read_reg (ctx->handle, reg, data, len);
	return ret;
}
int32_t iis3dwb_write_reg (stmdev_ctx_t* ctx, uint8_t reg, uint8_t* data, uint16_t len)
{
	int32_t ret;
	ret = ctx->write_reg (ctx->handle, reg, data, len);
	return ret;
}
float_t iis3dwb_from_lsb_to_celsius (int16_t lsb)
{
	return (((float_t)lsb / 256.0f) + 25.0f);
}
int32_t iis3dwb_xl_full_scale_set (stmdev_ctx_t *ctx, iis3dwb_fs_xl_t val)
{
	iis3dwb_ctrl1_xl_t ctrl1_xl;
	int32_t ret;

	ret = iis3dwb_read_reg (ctx, IIS3DWB_CTRL1_XL, (uint8_t*)&ctrl1_xl, 1);
	if (ret == 0)
	{
		ctrl1_xl.fs_xl = (uint8_t)val;
		ret = iis3dwb_write_reg (ctx, IIS3DWB_CTRL1_XL, (uint8_t*)&ctrl1_xl, 1);
	}
	return ret;
}
int32_t iis3dwb_xl_data_rate_set (stmdev_ctx_t *ctx, iis3dwb_odr_xl_t val)
{
	iis3dwb_ctrl1_xl_t ctrl1_xl;
	int32_t ret;

	ret = iis3dwb_read_reg (ctx, IIS3DWB_CTRL1_XL, (uint8_t*)&ctrl1_xl, 1);
	if (ret == 0)
	{
		ctrl1_xl.xl_en = (uint8_t)val;
		ret = iis3dwb_write_reg (ctx, IIS3DWB_CTRL1_XL, (uint8_t*)&ctrl1_xl, 1);
	}
	return ret;
}
int32_t iis3dwb_block_data_update_set (stmdev_ctx_t *ctx, uint8_t val)
{
	iis3dwb_ctrl3_c_t ctrl3_c;
	int32_t ret;

	ret = iis3dwb_read_reg (ctx, IIS3DWB_CTRL3_C, (uint8_t*)&ctrl3_c, 1);
	if(ret == 0)
	{
		ctrl3_c.bdu= (uint8_t)val;
		ret = iis3dwb_write_reg (ctx, IIS3DWB_CTRL3_C, (uint8_t*)&ctrl3_c, 1);
	}
	return ret;
}
int32_t iis3dwb_xl_axis_selection_set (stmdev_ctx_t *ctx, iis3dwb_xl_axis_sel_t val)
{
	iis3dwb_ctrl4_c_t ctrl4_c;
	iis3dwb_ctrl6_c_t ctrl6_c;
	int32_t ret;

	ret = iis3dwb_read_reg (ctx, IIS3DWB_CTRL4_C, (uint8_t*)&ctrl4_c, 1);
	if (ret == 0)
	{
		ctrl4_c._1ax_to_3regout = ( (uint8_t)val & 0x10U ) >> 4;
		ret = iis3dwb_write_reg (ctx, IIS3DWB_CTRL4_C, (uint8_t*)&ctrl4_c, 1);
	}
	if (ret == 0)
	{
		ret = iis3dwb_read_reg (ctx, IIS3DWB_CTRL6_C, (uint8_t*)&ctrl6_c, 1);
	}
	if (ret == 0)
	{
		ctrl6_c.xl_axis_sel = (uint8_t)val;
		ret = iis3dwb_write_reg (ctx, IIS3DWB_CTRL6_C, (uint8_t*)&ctrl6_c, 1);
	}
	return ret;
}
int32_t iis3dwb_xl_flag_data_ready_get (stmdev_ctx_t *ctx, uint8_t *val)
{
	iis3dwb_status_reg_t status_reg;
	int32_t ret;

	ret = iis3dwb_read_reg (ctx, IIS3DWB_STATUS_REG, (uint8_t*)&status_reg, 1);
	*val = status_reg.xlda;
	return ret;
}
int32_t iis3dwb_temp_flag_data_ready_get (stmdev_ctx_t *ctx, uint8_t *val)
{
	iis3dwb_status_reg_t status_reg;
	int32_t ret;

	ret = iis3dwb_read_reg (ctx, IIS3DWB_STATUS_REG, (uint8_t*)&status_reg, 1);
	*val = status_reg.tda;
	return ret;
}
int32_t iis3dwb_temperature_raw_get (stmdev_ctx_t *ctx, int16_t *val)
{
	uint8_t buff[2];
	int32_t ret;

	ret = iis3dwb_read_reg(ctx, IIS3DWB_OUT_TEMP_L, buff, 2);
	*val = (int16_t)buff[1];
	*val = (*val * 256) +  (int16_t)buff[0];
	return ret;
}
int32_t iis3dwb_acceleration_raw_get (stmdev_ctx_t *ctx, int16_t *val)
{
	uint8_t buff[6];
	int32_t ret;

	switch (qual_eixo)
	{
	case 0://X
		ret = iis3dwb_read_reg (ctx, IIS3DWB_OUTX_L_A, buff, 2);
		break;
	case 1://Y
		ret = iis3dwb_read_reg (ctx, IIS3DWB_OUTY_L_A, buff, 2);
		break;
	case 2://Z
		ret = iis3dwb_read_reg (ctx, IIS3DWB_OUTZ_L_A, buff, 2);
		break;
	}
	*val = (int16_t)buff[1];
	*val = (*val * 256) +  (int16_t)buff[0];
	return ret;
}
int32_t iis3dwb_device_id_get (stmdev_ctx_t *ctx, uint8_t *buff)
{
	int32_t ret;
	ret = iis3dwb_read_reg (ctx, IIS3DWB_WHO_AM_I, buff, 1);
	return ret;
}
int32_t iis3dwb_reset_set (stmdev_ctx_t *ctx, uint8_t val)
{
	iis3dwb_ctrl3_c_t ctrl3_c;
	int32_t ret;

	ret = iis3dwb_read_reg (ctx, IIS3DWB_CTRL3_C, (uint8_t*)&ctrl3_c, 1);
	if (ret == 0)
	{
		ctrl3_c.sw_reset = (uint8_t)val;
		ret = iis3dwb_write_reg (ctx, IIS3DWB_CTRL3_C, (uint8_t*)&ctrl3_c, 1);
	}
	return ret;
}
int32_t iis3dwb_reset_get (stmdev_ctx_t *ctx, uint8_t *val)
{
	iis3dwb_ctrl3_c_t ctrl3_c;
	int32_t ret;

	ret = iis3dwb_read_reg (ctx, IIS3DWB_CTRL3_C, (uint8_t*)&ctrl3_c, 1);
	*val = ctrl3_c.sw_reset;
	return ret;
}
int32_t iis3dwb_xl_filter_lp2_set (stmdev_ctx_t *ctx, uint8_t val)
{
	iis3dwb_ctrl1_xl_t ctrl1_xl;
	int32_t ret;

	ret = iis3dwb_read_reg (ctx, IIS3DWB_CTRL1_XL, (uint8_t*)&ctrl1_xl, 1);
	if (ret == 0)
	{
		ctrl1_xl.lpf2_xl_en = (uint8_t)val;
		ret = iis3dwb_write_reg (ctx, IIS3DWB_CTRL1_XL, (uint8_t*)&ctrl1_xl, 1);
	}
	return ret;
}
int32_t iis3dwb_xl_hp_path_on_out_set (stmdev_ctx_t *ctx, iis3dwb_hp_slope_xl_en_t val)
{
	iis3dwb_ctrl1_xl_t ctrl1_xl;
	iis3dwb_ctrl8_xl_t ctrl8_xl;
	int32_t ret;

	ret = iis3dwb_read_reg (ctx, IIS3DWB_CTRL1_XL, (uint8_t*)&ctrl1_xl, 1);
	if (ret == 0)
	{
		ctrl1_xl.lpf2_xl_en = ((uint8_t)val & 0x80U) >> 7;
		ret = iis3dwb_write_reg (ctx, IIS3DWB_CTRL1_XL, (uint8_t*)&ctrl1_xl, 1);
	}
	if (ret == 0)
	{
		ret = iis3dwb_read_reg (ctx, IIS3DWB_CTRL8_XL, (uint8_t*)&ctrl8_xl, 1);
	}
	if (ret == 0)
	{
		ctrl8_xl.fds = ((uint8_t)val & 0x10U) >> 4;
		ctrl8_xl.hp_ref_mode_xl = ((uint8_t)val & 0x20U) >> 5;
		ctrl8_xl.hpcf_xl = (uint8_t)val & 0x07U;
		ret = iis3dwb_write_reg  (ctx, IIS3DWB_CTRL8_XL, (uint8_t*)&ctrl8_xl, 1);
	}
	return ret;
}
//=================================================================================================
