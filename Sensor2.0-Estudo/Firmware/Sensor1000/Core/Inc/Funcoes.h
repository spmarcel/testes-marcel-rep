/*
 * Funcoes.h
 *
 *  Created on: Oct 13, 2021
 *      Author: Marcel Giraldi
 */

#ifndef INC_FUNCOES_H_
#define INC_FUNCOES_H_

//#define TENSAO_REG 3400.0
#define TENSAO_REG 3280.0

void calcula_bateria (void);
void calcula_temperatura (void);
void calcula_Vbat (void);
void Prepara_Envio_Normal (void);
void Preparar_Pacotes_full (void);
void Preparar_Envio_Duplo (void);
void delay_ms (uint16_t delay_ms);
void Transf_Pontos (void);

global uint32_t Res_NTC100k[39]
#ifdef DEF_VAR
={
	3068652,// 0 -40 graus
	2243556,
	1656340,// -30 graus
	1234196,
	 927801,// -20 graus
	 703379,
	 537563,// -10 graus
	 414025,
	 357012,// 0 graus
	 251044,
	 197528,// 10 graus
	 156446,
	 124697,// 20 graus
	 100000,
	  80669,// 30 graus
	  65447,
	  53390,// 40 graus
	  43787,
	  36097,// 50 graus
	  29906,
	  24898,// 20 60 graus
	  20825,
	  17499,// 70 graus
	  14769,
	  12519,// 80 graus
	  10656,
	   9107,// 90 graus
	   7814,
	   6731,// 100 graus
	   5819,
	   5050,// 30 110 graus
	   4398,
	   3843,// 120 graus
	   3370,
	   2128,// 130 graus // Prolongamento
	   1734,
	   1340,// 140 graus
		946,
		552 // 150 graus
}
#endif
;

#endif /* INC_FUNCOES_H_ */
