/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "main.h"
#include "stm32l4xx_hal.h"

typedef enum
{
  MC_STATE_STANDBY           =0x40,	/*!< STANDBY */
  MC_STATE_SLEEP             =0x36,	/*!< SLEEP */
  MC_STATE_READY             =0x03,	/*!< READY */
  MC_STATE_PM_SETUP          =0x3D,	/*!< PM_SETUP */
  MC_STATE_XO_SETTLING       =0x23,	/*!< XO_SETTLING */
  MC_STATE_SYNTH_SETUP       =0x53,	/*!< SYNT_SETUP */
  MC_STATE_PROTOCOL          =0x1F,	/*!< PROTOCOL */
  MC_STATE_SYNTH_CALIBRATION =0x4F,	/*!< SYNTH */
  MC_STATE_LOCK              =0x0F,	/*!< LOCK */
  MC_STATE_RX                =0x33,	/*!< RX */
  MC_STATE_TX                =0x5F,	/*!< TX */
  MC_STATE_LOCKWON           =0x13	/*!< LOCKWON */
} SpiritState;

typedef enum
{
  MODE_EXT_XO = 0,
  MODE_EXT_XIN = !MODE_EXT_XO
} ModeExtRef;

#define QTD_FREQB 26

typedef struct
{
  uint8_t Reg_num;
  uint8_t Reg_val[QTD_FREQB];
} Str_Spirit1;

const uint32_t FREQ_TAB[QTD_FREQB]
#ifdef DEF_VAR
=
	{150000, 151000, 152000, 153000, 154000, 155000, 156000, 157000, 158000, 159000,
	 160000, 161000, 162000, 163000, 164000, 165000, 166000, 167000, 168000, 169000,
	 170000, 171000, 172000, 173000, 173810, 174000}
#endif
;
//Reg.,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,173.81,174
const Str_Spirit1 Reg_Spirit1[7]
#ifdef DEF_VAR
={//        150	  151   152   153   154   155   156   157   158   159   160   161   162   163   164   165   166   167   168   169   170   171   172   173  17381  174
  //           0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15    16    17    18    19    20    21    22    23    24    25
	{0x08, {0x4B, 0x4B, 0x6B, 0x6B, 0x8B, 0x8B, 0xAC, 0xAC, 0xCC, 0xCC, 0xEC, 0xEC, 0x0C, 0x0C, 0x2C, 0x2C, 0x4C, 0x4C, 0x6C, 0x6D, 0x6D, 0x8D, 0x8D, 0xAD, 0xAD, 0xAD}},
	{0x09, {0x89, 0x9D, 0xB1, 0xC4, 0xD8, 0xEC, 0x00, 0x13, 0x27, 0x3B, 0x4E, 0x62, 0x76, 0x89, 0x9D, 0xB1, 0xC4, 0xD8, 0xEC, 0x00, 0x13, 0x27, 0x3B, 0x4E, 0x5E, 0x62}},
	{0x0A, {0xD8, 0x89, 0x3B, 0xEC, 0x9D, 0x4E, 0x00, 0xB1, 0x62, 0x13, 0xC4, 0x76, 0x27, 0xD8, 0x89, 0x3B, 0xEC, 0x9D, 0x4E, 0x00, 0xB1, 0x62, 0x13, 0xC4, 0xB8, 0x76}},
	{0x0B, {0x9D, 0xDD, 0x15, 0x4D, 0x8D, 0xC5, 0x05, 0x3D, 0x75, 0xB5, 0xED, 0x25, 0x65, 0x9D, 0xDD, 0x15, 0x4D, 0x8D, 0xC5, 0x05, 0x3D, 0x75, 0xB5, 0xED, 0x55, 0x25}},
	{0x0C, {0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01}},
	{0x9E, {0x5D, 0x5D, 0x5D, 0x5D, 0x5D, 0x5D, 0x5D, 0x5D, 0x5D, 0x5D, 0x5D, 0x5D, 0x5B, 0x5B, 0x5B, 0x5B, 0x5B, 0x5B, 0x5B, 0x5B, 0x5B, 0x5B, 0x5B, 0x5B, 0x5B, 0x5B}},
	{0x9F, {0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A}}
}
#endif
;

#define QTD_ODR 10

const uint32_t BR_TAB[QTD_ODR]
#ifdef DEF_VAR
={ 2400, 4800, 9600, 19200, 38400, 57600, 76800, 96000, 115200, 230400}
#endif
;
// 2400/4800/9600/19200/38400/57600/115200/230400, Reg. 0x1A, 0x1B, 0x1C e 0x1D
//   2400 =>  20k e  50k
//   4800 =>  20k e  50k
//   9600 =>  20k e  80k
//  19200 =>  20k e 120k
//  38400 =>  30k e 120k
//  57600 =>  40k e 180k
//  76800 =>  70k e 280k
//  96000 =>  70k e 240k
// 115200 =>  80k e 280k
// 230400 => 140k e 400k
const uint8_t Reg_Spirit1DR[4][QTD_ODR]
#ifdef DEF_VAR
= {//24    48    96    192   384   576   768   96    1152  2304
	{0x83, 0x83, 0x83, 0x83, 0x83, 0x22, 0x83, 0xE4, 0x22, 0x22},//0x1A
	{0x56, 0x57, 0x58, 0x59, 0x5A, 0x5B, 0x5B, 0x5B, 0x5C, 0x5D},//0x1B
	{0x55, 0x55, 0x55, 0x55, 0x61, 0x65, 0x73, 0x73, 0x75, 0x83},//0x1C
	{0x24, 0x24, 0x63, 0x03, 0x03, 0x42, 0x81, 0x02, 0x81, 0x21},//0x1D
}
#endif
;

extern HAL_StatusTypeDef SpiritSpiReadRegisters (uint8_t cRegAddress, uint8_t cNbBytes, uint8_t *pcBuffer);
extern HAL_StatusTypeDef SpiritSpiWriteRegisters (uint8_t cRegAddress, uint8_t cNbBytes, uint8_t* pcBuffer);
extern HAL_StatusTypeDef SdkEvalSpiReadFifo (uint16_t cNbBytes, uint8_t* pcBuffer);
extern HAL_StatusTypeDef SdkEvalSpiWriteFifo (uint16_t cNbBytes, uint8_t* pcBuffer);
extern HAL_StatusTypeDef SpiritSpiCommandStrobes (uint8_t cCommandCode);
extern void SpiritComputeXtalFrequency (void);
extern void SpiritExtraCurrent (void);
extern void SpiritIdentificationRFBoard (void);
extern void SpiritGeneralSetExtRef (ModeExtRef xExtMode);
extern void SpiritRangeExtInit (void);
uint8_t SpiritBaseConfiguration (uint32_t Qual_Freq, uint32_t Qual_BR);
uint8_t SpiritVcoCalibration (void);
extern HAL_StatusTypeDef Spirit1PktBasicSetPayloadLength (uint16_t nPayloadLength);
extern void SpiritTimerSetRxTimeoutMs (float fDesiredMsec);
extern void SpiritTimerComputeRxTimeoutValues (float fDesiredMsec , uint8_t* pcCounter , uint8_t* pcPrescaler);
extern void compara_registro (void);
extern uint8_t SpiritLinearFifoReadNumElementsRxFifo (void);
extern uint8_t SpiritQiGetRssi (void);


