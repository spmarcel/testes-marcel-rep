/*
 * Globais.h
 *
 *  Created on: 2 de ago de 2020
 *      Author: Marcel Giraldi
 *
 */

#ifndef INC_GLOBAIS_H_
#define INC_GLOBAIS_H_

#define global

#include <stdio.h>
#include "Defines.h"

typedef unsigned char byte;

#include "stm32l4xx_hal_spi.h"

global SPI_HandleTypeDef SPI_RF;

typedef struct
{
	uint8_t Scale;
	int16_t Valor_X;
	int16_t Valor_Y;
	int16_t Valor_Z;
	int16_t Valor_Temp;
	uint8_t Char_Return;
	uint8_t Char_Linefeed;
} RX_Struct;

global RX_Struct Set_RX;
global RX_Struct Set_RX_aux;

global const float SCALA_G[4]
#ifdef DEF_VAR
={ 0.061, 0.488, 0.122, 0.244}
#endif
;

global uint8_t Testar_Transmissao;
global uint8_t Estado_Leitura;

global uint8_t tttv_conf_RF;
global uint8_t tttv_conf2_RF;
global uint8_t tttv_res_RF;

global uint8_t pos_freq;
global uint8_t baud_rate;
global uint8_t RF_Recepcao;

global uint8_t timer_25ms;

global uint8_t TX_curto;
global uint8_t chegou_pct_curto;

global uint8_t pacotes_tx[NUM_DE_PACOTES+6];
global uint8_t pacotes_tx2[NUM_DE_PACOTES+6];
global uint8_t TX_buffer[110];
global uint8_t rx_buffer[NUM_DE_PACOTES+6][BYTES_TX+2];
global uint8_t rx2_buffer[NUM_DE_PACOTES+6][BYTES_TX+2];
global int16_t data_raw_acceleration[QTDE_PONTOS_MATRIZ];
global int16_t data_acceleration[QTDE_PONTOS_MATRIZ];
global uint8_t buffer_usb[16];
global uint8_t buffer_usb2[16];

global uint32_t pacotes_validos;
global uint16_t Pacotes_recebidos;
global uint8_t Aceita_Pacotes;
global uint8_t Flag_Led_USB;
global uint8_t id_sensor;
global uint16_t num_pacote;
global uint16_t num_pacote2;
global uint16_t num_pacote_int;
global float Gravidade_media;
global uint8_t imprime_pacotes;
global float Escala_aux;
global float Escala_const;
//global uint8_t Iniciou_Struct;
global int16_t numero_amostras;
global int16_t time_out_RX;
global uint16_t time_out_RF;
global int16_t time_ini_USB;
global uint8_t iniciou_USB;
global uint8_t time_segundo;
global int32_t I;
global int32_t I1;
global uint8_t Iusb;
global uint8_t case_tx;

global int32_t IT;
global int32_t IT1;
global int32_t IT2;

global uint32_t Time_processo;

global uint8_t receber_RF;

global uint8_t time_espera_pct;

global int32_t J;
global int32_t J1;

global int32_t K;
global int32_t K1;

global float V_minimo;
global float V_maximo;

global uint8_t qual_eixo;
global uint16_t flag_pacote;

//========================================================================================
// SINCRONA-MARCEL GIRALDI
#define CONST_MG	3600
#define FATOR_MG	(CONST_MG/60)
#define QTDE_MG		(NUM_DE_COLETAS/FATOR_MG)*2
#define PADRAO_MG	(QTDE_MG/AMPLITUDES_PCT)
#define TOTAL_MG	((PADRAO_MG+1)*AMPLITUDES_PCT)

//========================================================================================

global SPI_HandleTypeDef G_hspi2;

/* LLP configuration parameters */
#define EN_AUTOACK                      S_DISABLE
#define EN_PIGGYBACKING             	S_DISABLE
#define MAX_RETRANSMISSIONS         	PKT_DISABLE_RETX

#define HEADER_WRITE_MASK		0x00 /*!< Write mask for header byte*/
#define HEADER_READ_MASK		0x01 /*!< Read mask for header byte*/
#define HEADER_ADDRESS_MASK		0x00 /*!< Address mask for header byte*/
#define HEADER_COMMAND_MASK		0x80 /*!< Command mask for header byte*/

#define LINEAR_FIFO_ADDRESS		0xFF  /*!< Linear FIFO address*/

#define BUILT_HEADER(add_comm, w_r) (add_comm | w_r)  /*!< macro to build the header byte*/
#define WRITE_HEADER    BUILT_HEADER(HEADER_ADDRESS_MASK, HEADER_WRITE_MASK) /*!< macro to build the write header byte*/
#define READ_HEADER     BUILT_HEADER(HEADER_ADDRESS_MASK, HEADER_READ_MASK)  /*!< macro to build the read header byte*/
#define COMMAND_HEADER  BUILT_HEADER(HEADER_COMMAND_MASK, HEADER_WRITE_MASK) /*!< macro to build the command header byte*/

extern void Ler_Spirit1 (void);


#endif /* INC_GLOBAIS_H_ */

