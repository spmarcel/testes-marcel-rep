#include "spirit1_irq.h"
#include "spirit1.h"
#include "Defines.h"

HAL_StatusTypeDef Spirit1GpioIrqDeInit (void)
{
	uint8_t tmp[4] = {0x00,0x00,0x00,0x00};

	SpiritSpiWriteRegisters (IRQ_MASK3_BASE, 4, tmp);
	return HAL_OK;
}
HAL_StatusTypeDef Spirit1GpioIrqGetStatus (SpiritIrqs* pxIrqStatus)
{
	uint8_t tmp[4];
	uint8_t* pIrqPointer = (uint8_t*)pxIrqStatus;

	SpiritSpiReadRegisters (IRQ_STATUS3_BASE, 4, tmp);
	for (uint8_t i01 = 0; i01 < 4; i01++)
	{
		*pIrqPointer = tmp[3-i01];
		pIrqPointer++;
	}
	return HAL_OK;
}
HAL_StatusTypeDef Spirit1GpioIrqClearStatus (void)
{
	uint8_t tmp[4];

	SpiritSpiReadRegisters (IRQ_STATUS3_BASE, 4, tmp);
	return HAL_OK;
}
void SpiritGpioInit (SGpioInit* pxGpioInitStruct)
{
  uint8_t tempRegValue = 0x00;

  tempRegValue = ((uint8_t)(pxGpioInitStruct->xSpiritGpioMode) | (uint8_t)(pxGpioInitStruct->xSpiritGpioIO));
  SpiritSpiWriteRegisters (pxGpioInitStruct->xSpiritGpioPin, 1, &tempRegValue);
}
HAL_StatusTypeDef Spirit1GpioIrqConfig (IrqList xIrq, SFunctionalState xNewState)
{
	uint8_t tmpBuffer[4];
	uint32_t tempValue = 0;

	SpiritSpiReadRegisters (IRQ_MASK3_BASE, 4, tmpBuffer);

	for(uint8_t i01 = 0; i01 < 4; i01++)
	{
		tempValue += ((uint32_t)tmpBuffer[i01])<<(8*(3-i01));
	}
	if (xNewState == S_DISABLE)
	{
		tempValue &= (~xIrq);
	}
	else
	{
		tempValue |= (xIrq);
	}
	for (uint8_t j01 = 0; j01 < 4; j01++)
	{
		tmpBuffer[j01] = (uint8_t)(tempValue>>(8*(3-j01)));
	}
	SpiritSpiWriteRegisters (IRQ_MASK3_BASE, 4, tmpBuffer);
	return HAL_OK;
}
void SpiritCsma (SFunctionalState xNewState)
{
	uint8_t tmpReg;

	SpiritSpiReadRegisters (PROTOCOL1_BASE, 1, &tmpReg);
	if (xNewState == S_ENABLE)
	{
		tmpReg |= PROTOCOL1_CSMA_ON_MASK;
	}
	else
	{
		tmpReg &= ~PROTOCOL1_CSMA_ON_MASK;
	}
	SpiritSpiWriteRegisters (PROTOCOL1_BASE, 1, &tmpReg);
}


