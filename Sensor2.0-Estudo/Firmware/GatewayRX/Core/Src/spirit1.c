#include "spirit1.h"
#include "stm32l4xx_hal.h"
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include "Globais.h"
#include "Defines.h"

static uint32_t s_lXtalFrequency;

HAL_StatusTypeDef SpiritSpiReadRegisters (uint8_t cRegAddress, uint8_t cNbBytes, uint8_t *pcBuffer)
{
	uint8_t tx_buff[255] = {READ_HEADER, cRegAddress};
	uint8_t rx_buff[2];
	HAL_StatusTypeDef status;

	SPIRIT1_CS(0);
	HAL_SPI_TransmitReceive (&SPI_RF, tx_buff, rx_buff, 2, 1000);
	HAL_SPI_TransmitReceive (&SPI_RF, tx_buff, pcBuffer, cNbBytes, 1000);
	SPIRIT1_CS(1);
	((uint8_t*)&status)[1] = rx_buff[0];
	((uint8_t*)&status)[0] = rx_buff[1];
	return status;
}
HAL_StatusTypeDef SpiritSpiWriteRegisters (uint8_t cRegAddress, uint8_t cNbBytes, uint8_t* pcBuffer)
{
	uint8_t tx_buff[2] = {WRITE_HEADER, cRegAddress};
	uint8_t rx_buff[255];
	HAL_StatusTypeDef status;

	SPIRIT1_CS(0);
	HAL_SPI_TransmitReceive (&SPI_RF, tx_buff, rx_buff, 2, 1000);
	HAL_SPI_TransmitReceive (&SPI_RF, pcBuffer, &rx_buff[2], cNbBytes, 1000);
	SPIRIT1_CS(1);
	((uint8_t*)&status)[1] = rx_buff[0];
	((uint8_t*)&status)[0] = rx_buff[1];
	return status;
}
HAL_StatusTypeDef SdkEvalSpiReadFifo (uint16_t cNbBytes, uint8_t* pcBuffer)
{
	uint8_t tx_buff[96] = {READ_HEADER, LINEAR_FIFO_ADDRESS};
	uint8_t rx_buff[2];
	HAL_StatusTypeDef status;

	SPIRIT1_CS(0);
	HAL_SPI_TransmitReceive (&SPI_RF, tx_buff, rx_buff, 2, 1000);
	HAL_SPI_TransmitReceive (&SPI_RF, tx_buff, pcBuffer, cNbBytes, 1000);
	SPIRIT1_CS(1);
	((uint8_t*)&status)[1] = rx_buff[0];
	((uint8_t*)&status)[0] = rx_buff[1];
	return status;
}
HAL_StatusTypeDef SdkEvalSpiWriteFifo (uint16_t cNbBytes, uint8_t* pcBuffer)
{
	uint8_t tx_buff[2] = {WRITE_HEADER, LINEAR_FIFO_ADDRESS};
	uint8_t rx_buff[98];
	HAL_StatusTypeDef status;

	SPIRIT1_CS(0);
	HAL_SPI_TransmitReceive (&SPI_RF, tx_buff, rx_buff, 2, 100);
	HAL_SPI_TransmitReceive (&SPI_RF, pcBuffer, &rx_buff[2], cNbBytes, 100);
	SPIRIT1_CS(1);
	((uint8_t*)&status)[1] = rx_buff[0];
	((uint8_t*)&status)[0] = rx_buff[1];
	return status;
}
HAL_StatusTypeDef SpiritSpiCommandStrobes (uint8_t cCommandCode)
{
	uint8_t tx_buff[2] = {COMMAND_HEADER, cCommandCode};
	uint8_t rx_buff[2];
	HAL_StatusTypeDef status;

	SPIRIT1_CS(0);
	HAL_SPI_TransmitReceive (&SPI_RF, tx_buff, rx_buff, 2, 1000);
	SPIRIT1_CS(1);
	((uint8_t*)&status)[1] = rx_buff[0];
	((uint8_t*)&status)[0] = rx_buff[1];
	return status;
}
void SpiritComputeXtalFrequency (void)
{
	s_lXtalFrequency = 26000000;
}
void SpiritExtraCurrent (void)
{
	uint8_t tmp[7];

	// Extra current in after power on fix.
	tmp[0] = 0xCA;
	SpiritSpiWriteRegisters (0xB2, 1, tmp);
	tmp[0] = 0x04;
	SpiritSpiWriteRegisters (0xA8, 1, tmp);
	SpiritSpiReadRegisters (0xA8, 1, tmp);
	tmp[0] = 0x00;
	SpiritSpiWriteRegisters (0xA8, 1, tmp);
}
void SpiritIdentificationRFBoard (void)
{
	uint8_t state;

	do
	{
		for (uint8_t i01 = 0; i01 < 255; i01++);
		SpiritSpiReadRegisters (0xC1, 1, &state);
	}
	while ((state&0xFE) != 0x06); /* wait until READY */
    SpiritComputeXtalFrequency ();
}
void SpiritGeneralSetExtRef (ModeExtRef xExtMode)
{
	uint8_t tempRegValue;

	SpiritSpiReadRegisters (ANA_FUNC_CONF0_BASE, 1, &tempRegValue);
	if (xExtMode == MODE_EXT_XO)
	{
		tempRegValue &= ~EXT_REF_MASK;
	}
	else
	{
		tempRegValue |= EXT_REF_MASK;
	}
	SpiritSpiWriteRegisters (ANA_FUNC_CONF0_BASE, 1, &tempRegValue);
}
void SpiritRangeExtInit (void)
{
    SpiritGeneralSetExtRef (MODE_EXT_XIN);
    uint8_t tmp = 0x01;
    SpiritSpiWriteRegisters (0xB6, 1, &tmp);
}
uint8_t SpiritBaseConfiguration (uint32_t Qual_Freq, uint32_t Qual_BR)
{
	uint8_t tmp[16];
	uint8_t state;

	SpiritSpiCommandStrobes (COMMAND_SRES);
	tmp[0] = 0xCA;
	SpiritSpiWriteRegisters (0xB2, 1, tmp);
	tmp[0] = 0x04;
	SpiritSpiWriteRegisters (0xA8, 1, tmp);
	SpiritSpiReadRegisters (0xA8, 1, tmp);
	tmp[0] = 0x00;
	SpiritSpiWriteRegisters (0xA8, 1, tmp);
	tmp[0] = 0x29;
	tttv_conf2_RF = 2;
	SpiritSpiCommandStrobes (COMMAND_STANDBY);
	do
	{
		SpiritSpiReadRegisters (0xC1, 1, &state);
		if (tttv_conf2_RF)
		{
			tttv_conf2_RF--;
		}
		else
		{
			return 0;
		}
	}
	while ((state&0xFE) != 0x80); /* wait until STANDBY */
	tttv_conf2_RF = 2;
	SpiritSpiWriteRegisters (0xB4, 1, tmp); /* Enable divider */
	SpiritSpiCommandStrobes (COMMAND_READY);
	do
	{
		SpiritSpiReadRegisters (0xC1, 1, &state);
		if (tttv_conf2_RF)
		{
			tttv_conf2_RF--;
		}
		else
		{
			return 0;
		}
	}
	while ((state&0xFE) != 0x06); /* wait until READY */
	pos_freq = 19; // 169000MHz
	for (I = 0; I < QTD_FREQB; I++)
	{
		if (Qual_Freq == FREQ_TAB[I])
		{
			pos_freq = I;
			break;
		}
	}
	baud_rate = 4; // 38400
	for (I = 0; I < QTD_ODR; I++)
	{
		if (Qual_BR == BR_TAB[I])
		{
			baud_rate = I;
			break;
		}
	}
	// Registros para Frequencia Base
	tmp[0] = Reg_Spirit1[0].Reg_val[pos_freq]; /* reg. SYNT3 (0x08) */
	tmp[1] = Reg_Spirit1[1].Reg_val[pos_freq]; /* reg. SYNT2 (0x09) */
	tmp[2] = Reg_Spirit1[2].Reg_val[pos_freq]; /* reg. SYNT1 (0x0A) */
	tmp[3] = Reg_Spirit1[3].Reg_val[pos_freq]; /* reg. SYNT0 (0x0B) */
	tmp[4] = Reg_Spirit1[4].Reg_val[pos_freq]; /* reg. CH_SPACE (0x0C) */
	SpiritSpiWriteRegisters (Reg_Spirit1[0].Reg_num, 5, tmp);
	// ==============================================================================
	tmp[0] = 0x11; /* reg. PA_POWER[8] (0x10)  está em 7dBm igaul kit ST*/
	SpiritSpiWriteRegisters(0x10, 1, tmp);
	// ==============================================================================
	// Registros para Data Rate4
	tmp[0] = Reg_Spirit1DR[0][baud_rate]; /* reg. MOD1 (0x1A) */
	tmp[1] = Reg_Spirit1DR[1][baud_rate]; /* reg. MOD0 (0x1B) */
	tmp[2] = Reg_Spirit1DR[2][baud_rate]; /* reg. FDEV0 (0x1C)*/
	tmp[3] = Reg_Spirit1DR[3][baud_rate]; /* reg. CHFLT (0x1D)*/
	// ------------------------------
	tmp[4] = 0xC8; /* reg. AFC2 (0x1E) */
	SpiritSpiWriteRegisters (0x1A, 5, tmp); // grava reg. 0x1A/B/C/D/E
	// ==============================================================================
	tmp[0] = 0x62; /* reg. AGCCTRL1 (0x25) */
	SpiritSpiWriteRegisters (0x25, 1, tmp);
	tmp[0] = 0x15; /* reg. ANT_SELECT_CONF (0x27) */
	SpiritSpiWriteRegisters (0x27, 1, tmp);
	tmp[0] = 0x3F; /* reg. PCKTCTRL2 (0x32) */
	tmp[1] = 0x30; /* reg. PCKTCTRL1 (0x33) */
	SpiritSpiWriteRegisters (0x32, 2, tmp);
	tmp[0] = 0x41; /* reg. PCKT_FLT_OPTIONS (0x4F) */
	tmp[1] = 0x40; /* reg. PROTOCOL[2] (0x50) */
	tmp[2] = 0x01; /* reg. PROTOCOL[1] (0x51) */
	SpiritSpiWriteRegisters (0x4F, 3, tmp);
	tmp[0] = 0x00; /* reg. RCO_VCO_CALIBR_IN[1] (0x6E) */
	tmp[1] = 0x00; /* reg. RCO_VCO_CALIBR_IN[0] (0x6F) */
	SpiritSpiWriteRegisters (0x6E, 2, tmp);
	// ==============================================================================
	tmp[0] = Reg_Spirit1[5].Reg_val[pos_freq]; /* reg. SYNTH_CONFIG[1] (0x9E) */
	tmp[1] = Reg_Spirit1[6].Reg_val[pos_freq]; /* reg. SYNTH_CONFIG[0] (0x9F) */
	SpiritSpiWriteRegisters (Reg_Spirit1[5].Reg_num, 2, tmp);
	// ==============================================================================
	tmp[0] = 0x25; /* reg. VCO_CONFIG (0xA1) */
	SpiritSpiWriteRegisters (0xA1, 1, tmp);
	tmp[0] = 0x35; /* reg. DEM_CONFIG (0xA3) */
	SpiritSpiWriteRegisters (0xA3, 1, tmp);
	tmp[0] = 0x22;
	SpiritSpiWriteRegisters (0xBC, 1, tmp);
	// ==============================================================================
	return 1;
}
uint8_t SpiritVcoCalibration (void)
{
	uint8_t tmp[4];
	uint8_t cal_words[2];
	uint8_t state;

	tmp[0] = 0x25;
	SpiritSpiWriteRegisters (0xA1,1,tmp); /* increase VCO current (restore to 0x11) */
	SpiritSpiReadRegisters (0x50,1,tmp);
	tmp[0] |= 0x02;
	SpiritSpiWriteRegisters (0x50,1,tmp); /* enable VCO calibration (to be restored) */
	tttv_conf2_RF = 2;
	SpiritSpiCommandStrobes (COMMAND_LOCKTX);
	do
	{
		SpiritSpiReadRegisters (0xC1, 1, &state);
		if (tttv_conf2_RF)
		{
			tttv_conf2_RF--;
		}
		else
		{
			return 0;
		}
	}
	while ((state&0xFE) != 0x1E); /* wait until LOCK (MC_STATE = 0x0F <<1) */
	SpiritSpiReadRegisters (0xE5, 1, &cal_words[0]); /* calib out word for TX */
	tttv_conf2_RF = 2;
	SpiritSpiCommandStrobes (COMMAND_READY);
	do
	{
		SpiritSpiReadRegisters (0xC1, 1, &state);
		if (tttv_conf2_RF)
		{
			tttv_conf2_RF--;
		}
		else
		{
			return 0;
		}
	}
	while ((state&0xFE) != 0x06); /* wait until READY (MC_STATE = 0x03 <<1) */
	tttv_conf2_RF = 2;
	SpiritSpiCommandStrobes (COMMAND_LOCKRX);
	do
	{
		SpiritSpiReadRegisters (0xC1, 1, &state);
		if (tttv_conf2_RF)
		{
			tttv_conf2_RF--;
		}
		else
		{
			return 0;
		}
	}
	while ((state&0xFE) != 0x1E); /* wait until LOCK (MC_STATE = 0x0F <<1) */
	SpiritSpiReadRegisters (0xE5, 1, &cal_words[1]); /* calib out word for RX */
	tttv_conf2_RF = 2;
	SpiritSpiCommandStrobes (COMMAND_READY);
	do
	{
		SpiritSpiReadRegisters (0xC1, 1, &state);
		if (tttv_conf2_RF)
		{
			tttv_conf2_RF--;
		}
		else
		{
			return 0;
		}
	}
	while ((state&0xFE) != 0x06); /* wait until READY (MC_STATE = 0x03 <<1) */
	SpiritSpiReadRegisters (0x50,1,tmp);
	tmp[0] &= 0xFD;
	SpiritSpiWriteRegisters (0x50,1,tmp); /* VCO calib restored to 0 */
	SpiritSpiWriteRegisters (0x6E,2,cal_words); /* write both calibration words */
	return 1;
}
HAL_StatusTypeDef Spirit1PktBasicSetPayloadLength (uint16_t nPayloadLength)
{
	uint8_t tmpBuffer[2];

	tmpBuffer[0] = (uint8_t)(nPayloadLength>>8);
	tmpBuffer[1] = (uint8_t)nPayloadLength;
	SpiritSpiWriteRegisters (PCKTLEN1_ADDR, 2, tmpBuffer);
	return HAL_OK;
}
void SpiritTimerSetRxTimeoutMs (float fDesiredMsec)
{
  uint8_t tempRegValue[2];

  SpiritTimerComputeRxTimeoutValues (fDesiredMsec , &tempRegValue[1] , &tempRegValue[0]);
  SpiritSpiWriteRegisters (TIMERS5_RX_TIMEOUT_PRESCALER_BASE, 2, tempRegValue);
}
void SpiritTimerComputeRxTimeoutValues (float fDesiredMsec , uint8_t* pcCounter , uint8_t* pcPrescaler)
{
	uint32_t nXtalFrequency = 26000000;
	uint32_t n;
	float err;

	n = (uint32_t)(fDesiredMsec*nXtalFrequency/1210000);
	if (n/0xFF > 0xFD)
	{
		(*pcCounter) = 0xFF;
		(*pcPrescaler) = 0xFF;
		return;
	}
	(*pcPrescaler) = (n/0xFF)+2;
	(*pcCounter) = (n/(*pcPrescaler));
	err= S_ABS ((float)(*pcCounter)*(*pcPrescaler)*1210000/nXtalFrequency-fDesiredMsec);
	if ((*pcCounter) <= 254)
	{
		if (S_ABS ((float)((*pcCounter)+1)*(*pcPrescaler)*1210000/nXtalFrequency-fDesiredMsec)<err)
		{
			(*pcCounter) = (*pcCounter)+1;
		}
	}
	(*pcPrescaler)--;
	if ((*pcCounter) > 1)
	{
		(*pcCounter)--;
	}
	else
	{
		(*pcCounter) = 1;
	}
}
uint8_t SpiritLinearFifoReadNumElementsRxFifo (void)
{
	uint8_t tempRegValue;

	SpiritSpiReadRegisters (LINEAR_FIFO_STATUS0_BASE, 1, &tempRegValue);
	return (tempRegValue & 0x7F);
}
uint8_t SpiritQiGetRssi (void)
{
	uint8_t tempRegValue;

	SpiritSpiReadRegisters (RSSI_LEVEL_BASE, 1, &tempRegValue);
	return tempRegValue;
}

