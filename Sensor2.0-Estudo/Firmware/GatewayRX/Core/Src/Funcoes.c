/*
 * Funcoes.c
 *
 *  Created on: 18 de out de 2021
 *      Author: Marcel
 */

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h> //for va_list var arg functions
#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_conf.h"
#include "stm32l4xx_hal_spi.h"
#include "usbd_cdc_if.h"

#include <time.h>

#include "Globais.h"
#include "spirit1.h"
#include "spirit1_irq.h"
#include "Funcoes.h"

void Transf_Pontos (void)
{
	float V_aux1;
	int16_t gravidade;
	int16_t aux_int;

	IT1 = 16;
	// valor mínimo
	aux_int = (int16_t)rx_buffer[0][IT1+1];
	gravidade = (aux_int * 256) + (int16_t)rx_buffer[0][IT1];
	V_aux1 = (((float)(gravidade))*Escala_aux + Escala_const);
	V_minimo = V_aux1;
	// valor médio
	aux_int = (int16_t)rx_buffer[0][IT1+3];
	gravidade = (aux_int * 256) + (int16_t)rx_buffer[0][IT1+2];
	V_aux1 = (((float)(gravidade))*Escala_aux + Escala_const);
	Gravidade_media = V_aux1;
	// valor máximo
	aux_int = (int16_t)rx_buffer[0][IT1+5];
	gravidade = (aux_int * 256) + (int16_t)rx_buffer[0][IT1+4];
	V_aux1 = (((float)(gravidade))*Escala_aux + Escala_const);
	V_maximo = V_aux1;

	Pacotes_recebidos = 0;
	for (I = 2; I <= (NUM_DE_PACOTES+2); I++)
	{
		// Verificar se algum pacote veio como certo e está zerado
		if (pacotes_tx[I])
		{
			K1 = 0;
			Pacotes_recebidos++;
			for (K = 0; K < QTD_BYTES_RF; K++)
			{
				if (rx_buffer[I-1][K] == 0x00) K1++;
			}
			if (K1 > (QTD_BYTES_RF/2))
			{
				Pacotes_recebidos--;
				pacotes_tx[I] = 0;
			}
		}
	}
	num_pacote = 1;
	num_pacote2 = NUM_DE_PACOTES/2;
	J = 0;
	IT1 = 0;
	case_tx = TIPO_ENVIO;
	switch (case_tx)
	{
	case TX_NORMAL:
		for (I = 0; I < NUM_DE_COLETAS; I++)
		{
			if (pacotes_tx[num_pacote+1]) // vai de 2 até 267
			{// Tem que somar 1 nos pacotes_tx, o indice 1 é do cabeçalho
				// No buffer pacotes_tx registra 1 se o pacote veio e 0 se não veio
				// Neste caso o indice começa do 1 pois o pacote 1 é o cabeçalho
				//=================================================================
				// No Buffer rx_buffer o indice 1 é o início dos pacotes
				// o indice 0 é o cabeçalho
				aux_int = (int16_t)rx_buffer[num_pacote][IT1+1];
				gravidade = (aux_int * 256) + (int16_t)rx_buffer[num_pacote][IT1];
				data_raw_acceleration[J] = gravidade;
				data_acceleration[J] = gravidade;
				J++;
			}
			else
			{// Estouro/Null
				data_raw_acceleration[J] = 32765;
				data_acceleration[J] = 32765;
				J++;
			}
			IT1 += 2;
			if (IT1 >= QTD_BYTES_RF)
			{
				num_pacote++;
				IT1 = 0;
			}
		}
		break;
	case TX_DUPLO:
		for (I = 0; I < (NUM_DE_COLETAS/2); I++)
		{
			if (pacotes_tx[num_pacote+1]) // vai de 2 até 267
			{// Tem que somar 1 nos pacotes_tx, o indice 1 é do cabeçalho
				// No buffer pacotes_tx registra 1 se o pacote veio e 0 se não veio
				// Neste caso o indice começa do 1 pois o pacote 1 é o cabeçalho
				//=================================================================
				// No Buffer rx_buffer o indice 1 é o início dos pacotes
				// o indice 0 é o cabeçalho
				aux_int = (int16_t)rx_buffer[num_pacote][IT1+1];
				gravidade = (aux_int * 256) + (int16_t)rx_buffer[num_pacote][IT1];
				data_raw_acceleration[J] = gravidade;
				J++;
			}
			else
			{
				if (pacotes_tx[num_pacote+num_pacote2+1]) // vai de 268 até 534
				{// Tem que somar 1 nos pacotes_tx, o indice 1 é do cabeçalho
					// No Buffer rx_buffer o indice 1 é o início dos pacotes
					// o indice 0 é o cabeçalho
					aux_int = (int16_t)rx_buffer[num_pacote+num_pacote2][IT1+1];
					gravidade = (aux_int * 256) + (int16_t)rx_buffer[num_pacote+num_pacote2][IT1];
					data_raw_acceleration[J] = gravidade;
				}
				else
				{
					data_raw_acceleration[J] = 32765;
				}
				J++;
			}
			IT1 += 2;
			if (IT1 >= QTD_BYTES_RF)
			{
				num_pacote++;
				IT1 = 0;
			}
		}
		Preparar_Pacotes_full ();
		break;
	case TX_FULL:
		for (I = 0; I < NUM_DE_COLETAS; I++)
		{
			if (pacotes_tx[num_pacote+1]) // vai de 2 até 267
			{// Tem que somar 1 nos pacotes_tx, o indice 1 é do cabeçalho
				// No buffer pacotes_tx registra 1 se o pacote veio e 0 se não veio
				// Neste caso o indice começa do 1 pois o pacote 1 é o cabeçalho
				//=================================================================
				// No Buffer rx_buffer o indice 1 é o início dos pacotes
				// o indice 0 é o cabeçalho
				aux_int = (int16_t)rx_buffer[num_pacote][IT1+1];
				gravidade = (aux_int * 256) + (int16_t)rx_buffer[num_pacote][IT1];
				data_acceleration[J] = gravidade;
				J++;
			}
			else
			{
				data_acceleration[J] = 32765;
				J++;
			}
			IT1 += 2;
			if (IT1 >= QTD_BYTES_RF)
			{
				num_pacote++;
				IT1 = 0;
			}
		}
		break;
	}
}
void Preparar_Pacotes_full (void)
{
	int16_t aux_data_ACC;

	//==============================================================================
	// Preparar
	// Aqui já tenho os valores das gravidades
	J = 0;
	for (I = 0; I < (NUM_DE_COLETAS/2); I++)
	{ // Tem que ser (NUM_DE_COLETAS/2)
		//================================================================================
		data_acceleration[J] = data_raw_acceleration[I];
		J++;
		if (I < ((NUM_DE_COLETAS/2)-1))
		{
			if (data_raw_acceleration[I] > data_raw_acceleration[I+1])
			{// Atual é maior que o próximo
				aux_data_ACC = (data_raw_acceleration[I+1] + ((data_raw_acceleration[I]-data_raw_acceleration[I+1])/2));
				if (aux_data_ACC > data_raw_acceleration[I]) aux_data_ACC = data_raw_acceleration[I];
				if (aux_data_ACC < data_raw_acceleration[I+1]) aux_data_ACC = data_raw_acceleration[I+1];
			}
			else
			{// Atual é menor que o próximo
				aux_data_ACC = (data_raw_acceleration[I] + ((data_raw_acceleration[I+1]-data_raw_acceleration[I])/2));
				if (aux_data_ACC < data_raw_acceleration[I]) aux_data_ACC = data_raw_acceleration[I];
				if (aux_data_ACC > data_raw_acceleration[I+1]) aux_data_ACC = data_raw_acceleration[I+1];
			}
			data_acceleration[J] = aux_data_ACC;
			J++;
		}
	}
	asm("NOP");
}
