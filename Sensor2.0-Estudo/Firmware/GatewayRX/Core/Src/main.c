/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  *
  * Firmware enxuto para a Placa Sensor 2.0 - 1000 unidades
  * Início: 14/12/2021
  * Autor: Eng. Marcel Giraldi
  *
  * Versão: 1.00.01 - Gateway-RX
  *
  * Obs.: Neste projeto só tem o que foi homologado do firmware/hardware
  *
  * Para escrever no main.c e alguns outros arquivos, teremos que obedecer o
  * inicio e fim de onde pode escrever
  * o inicio vem com: "USER CODE BEGIN"
  * o fim vem com "USER CODE END"
  * por exemplo Header:
  *
  * USER CODE BEGIN Header
  *
  * aqui dentro destas instruções você poderá escrever seu código, fora destas
  * instruções quaiquer configuração que fizer no projeto ao salvar a IDE vai
  * apagar o que foi feito fora destas instruções
  *
  * USER CODE END Header
  *
  * Histórico de correções
  * MG27/12/21 - Inicio da construção da Versão V0 - V1.00.01
  * MG03/01/22 - Colocado informação de dBm que o RX recebe - V1.00.02
  * MG07/01/22 - Correção da indicação de dBm - V1.00.03
  *
  *
  *
  *
  *
  *
  * **** Lembrar de alterar a Versão do Firmware na variável Versao[]
  *
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#define DEF_VAR
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdarg.h> //for va_list var arg functions
#include <string.h>
#include "time.h"
#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_conf.h"
#include "stm32l4xx_hal_spi.h"
#include "usbd_cdc_if.h"

#include "Globais.h"
#include "Funcoes.h"
#include "Defines.h"
#include "spirit1.h"
#include "spirit1_IRQ.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define RSD_TX 1
#define RSD_WAIT_TX_FLAG 2
#define RSD_RX 3
#define RSD_RECEIVED_DATA_READY 4
#define RSD_IDLE 5
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
RTC_HandleTypeDef hrtc;

SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim2;

/* USER CODE BEGIN PV */

//VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

uint8_t Versao[] = "V1.00.06";

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV

SGpioInit xGpioIRQ3 = {
	SPIRIT_GPIO_3,
	SPIRIT_GPIO_MODE_DIGITAL_OUTPUT_LP,
	SPIRIT_GPIO_DIG_OUT_IRQ
};
SpiritIrqs xSpirit1IRQ;

FlagStatus xTxDoneFlag = RESET;
FlagStatus xRxDoneFlag = RESET;

uint32_t BAUD_RATE;
uint32_t FREQUENCIA_TX;
uint32_t FREQUENCIA_RX;
uint32_t FREQUENCIA_RX_VAL;

//uint8_t counter;
uint8_t vectcTxBuff[110];
uint8_t vectcRxBuff[110];
uint8_t spiritstate;
uint32_t cRxData;

float Valor_dBm;
uint32_t qtd_Valor_dBm;


// Variáveis RTC
RTC_TimeTypeDef sTime2 = {0};
RTC_DateTypeDef sDate2 = {0};

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_RTC_Init(void);
static void MX_SPI1_Init(void);
static void MX_TIM2_Init(void);
/* USER CODE BEGIN PFP */
void Convert_to_int (float val_float);
void Find_Line_Feed (void);
void Arquivo_JSON (void);
void recuperar_pct (void);
void Ajusta_Pacotes (void);
void Verifica_Pacotes (void);
void routine_spirit_driver (void);
void Iniciar_Spirit1 (uint8_t RX_TX, uint32_t Freq_RX_TX);
void Desliga_Spirit1 (void);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_RTC_Init();
  MX_SPI1_Init();
  MX_TIM2_Init();
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN 2 */
    //=============================================================================================
    // Inicia variáveis
	memset (&pacotes_tx, 0x00, NUM_DE_PACOTES);
	time_ini_USB = 4*tickseg;
	iniciou_USB = 0;
	num_pacote = 0;
	Gravidade_media = CONST_GRAVIDADE;
	receber_RF = 1;
	time_espera_pct = 0;
	RF_Recepcao = 1;
	//====================================================================================
	// Inicia Timer Interrupt
	HAL_TIM_Base_Start_IT (&htim2);
	//====================================================================================
	// Modo de transmissão
	// 2400, 4800, 9600, 19200, 38400, 57600, 76800, 96000, 115200 e 230400

	BAUD_RATE = 38400;

	// 150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,173.81,174
	// Tem que colocar em kHz, 150MHz tem que colocar 150000

	FREQUENCIA_TX = 168000;
	FREQUENCIA_RX = 168000;
	FREQUENCIA_RX_VAL = 168000;

	//====================================================================================

	Testar_Transmissao = 0;
	TX_curto = 0;

	//====================================================================================

	//====================================================================================
	SPI_RF = hspi1;
	//====================================================================================
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	chegou_pct_curto = 0;
	Estado_Leitura = EST_GATEWAYRX_INIT;
	time_out_RF = tickseg;
  while (1)
  {
	switch (Estado_Leitura)
	{
	case EST_GATEWAYRX_INIT:
		if (!time_out_RF)
		{
			//====================================================================================
			// Inicia Spirit1
			Iniciar_Spirit1 (SPIRIT1_RX, FREQUENCIA_RX);
			//====================================================================================
			for (I = 0; I <= (NUM_DE_PACOTES+2); I++)
			{
				memset (&rx_buffer[I], 0x00, BYTES_TX+2);
			}
			memset (&pacotes_tx, 0x00, NUM_DE_PACOTES+4);
			num_pacote = 0;
			num_pacote_int = 0;
			Aceita_Pacotes = 0;
			Valor_dBm = 0;
			qtd_Valor_dBm = 0;
			Flag_Led_USB = 0;
			HAL_GPIO_WritePin (PWM_USB_GPIO_Port, PWM_USB_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin (PWM_RF_GPIO_Port, PWM_RF_Pin, GPIO_PIN_RESET);
			receber_RF = 1;
			chegou_pct_curto = 0;
			if (Estado_Leitura != EST_DORMIR)
			{
				if (TX_curto)
				{
					Estado_Leitura = EST_GATEWAYRX_READ2;
				}
				else
				{
					Estado_Leitura = EST_GATEWAYRX_READ;
				}
			}
		}
		break;
	case EST_GATEWAYRX_READ:
		routine_spirit_driver ();
		if ((num_pacote_int) && (time_out_RX > (5*tickseg)))// Este time-out tem que ser maior que o time-out timeout_RX_Radio
		{//Aqui é só se não estiver zerado a Flag_RX depois de 15 segundos que recebeu algo.
			HAL_GPIO_WritePin (PWM_RF_GPIO_Port, PWM_RF_Pin, GPIO_PIN_RESET);
			if ((pacotes_validos >= NUM_DE_PACOTES/5) || (pacotes_tx2[1]))
			{// 20% dos pacotes vieram
				Aceita_Pacotes = 1;
			}
			else
			{
				Aceita_Pacotes = 0;
				Estado_Leitura = EST_DORMIR;
			}
			pacotes_validos = 0;
			time_out_RX = 0;
		}
		imprime_pacotes = 0;
		if (((num_pacote_int >= (NUM_DE_PACOTES+1)) || (Aceita_Pacotes)) && (!time_espera_pct))
		{// Ou Flag_RX é maior igual que o número total de amostra ou começou a receber e passou de 12 segundos e não atigiu o NUM_BYTES
			receber_RF = 0;
			for (I = 0; I < NUM_DE_PACOTES+3; I++)
			{
				memcpy (&rx_buffer[I][0], &rx2_buffer[I][0], BYTES_TX+2);
				memset (&rx2_buffer[I], 0x00, BYTES_TX+2);
			}
			memcpy (&pacotes_tx, &pacotes_tx2, NUM_DE_PACOTES+4);
			memset (&pacotes_tx2, 0x00, NUM_DE_PACOTES+4);
			HAL_GPIO_WritePin (PWM_RF_GPIO_Port, PWM_RF_Pin, GPIO_PIN_RESET);
			pacotes_validos = 0;
			Aceita_Pacotes = 0;
			num_pacote_int = 0;
			Flag_Led_USB = 1;
			memset (&TX_buffer, 0x00, BYTES_TX+2);// Buffer para enviar pela USB
			rx_buffer[0][0] = 0x1B;
			rx_buffer[0][1] = 0x1B;
			Arquivo_JSON ();
			Flag_Led_USB = 0;
			HAL_GPIO_WritePin (PWM_USB_GPIO_Port, PWM_USB_Pin, GPIO_PIN_RESET);
			Estado_Leitura = EST_DORMIR;
		}
		break;
	case EST_GATEWAYRX_READ2:
		routine_spirit_driver ();
		if (chegou_pct_curto)
		{
			receber_RF = 0;
			for (I = 0; I < NUM_DE_PACOTES+3; I++)
			{
				memcpy (&rx_buffer[I][0], &rx2_buffer[I][0], BYTES_TX+2);
				memset (&rx2_buffer[I], 0x00, BYTES_TX+2);
			}
			HAL_GPIO_WritePin (PWM_RF_GPIO_Port, PWM_RF_Pin, GPIO_PIN_RESET);
			memset (&TX_buffer, 0x00, BYTES_TX+2);// Buffer para enviar pela USB
			rx_buffer[0][0] = 0x1B;
			rx_buffer[0][1] = 0x1B;
			Arquivo_JSON ();
			HAL_GPIO_WritePin (PWM_USB_GPIO_Port, PWM_USB_Pin, GPIO_PIN_RESET);
			chegou_pct_curto = 0;
			Estado_Leitura = EST_DORMIR;
		}
		break;
	case EST_DORMIR:
		Desliga_Spirit1 ();
		time_out_RF = 2*tickseg;
		Estado_Leitura = EST_GATEWAYRX_INIT;
		break;
	default:
		Estado_Leitura = EST_DORMIR;
		break;
	}
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 20;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};
  RTC_AlarmTypeDef sAlarm = {0};

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */
  /** Initialize RTC Only
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */

  /* USER CODE END Check_RTC_BKUP */

  /** Initialize RTC and set the Time and Date
  */
  sTime.Hours = 8;
  sTime.Minutes = 0;
  sTime.Seconds = 0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }
  sDate.WeekDay = RTC_WEEKDAY_MONDAY;
  sDate.Month = RTC_MONTH_DECEMBER;
  sDate.Date = 14;
  sDate.Year = 21;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable the Alarm A
  */
  sAlarm.AlarmTime.Hours = 0;
  sAlarm.AlarmTime.Minutes = 0;
  sAlarm.AlarmTime.Seconds = 0;
  sAlarm.AlarmTime.SubSeconds = 0;
  sAlarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sAlarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
  sAlarm.AlarmMask = RTC_ALARMMASK_NONE;
  sAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
  sAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
  sAlarm.AlarmDateWeekDay = 1;
  sAlarm.Alarm = RTC_ALARM_A;
  if (HAL_RTC_SetAlarm_IT(&hrtc, &sAlarm, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */
  // Este código acima é defaut da IDE, não adianta tirar que volta quando mudar alguma configuração
  /* USER CODE END RTC_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 7;
  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 799;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 99;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, SPIRIT1_SDN_Pin|SPIRIT1_CS_Pin|REF_VFB_Pin|PWM_Timer_Pin
                          |PWM_RF_Pin|PWM_USB_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, AMPL_CTX_Pin|AMPL_BYP_Pin|AMPL_CSD_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(PWR_RF_GPIO_Port, PWR_RF_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : SPIRIT1_SDN_Pin */
  GPIO_InitStruct.Pin = SPIRIT1_SDN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(SPIRIT1_SDN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : SPIRIT1_CS_Pin REF_VFB_Pin PWM_Timer_Pin PWM_USB_Pin */
  GPIO_InitStruct.Pin = SPIRIT1_CS_Pin|REF_VFB_Pin|PWM_Timer_Pin|PWM_USB_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : SPIRIT1_GPIO0_Pin SPIRIT1_GPIO1_Pin */
  GPIO_InitStruct.Pin = SPIRIT1_GPIO0_Pin|SPIRIT1_GPIO1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : SPIRIT1_GPIO2_Pin */
  GPIO_InitStruct.Pin = SPIRIT1_GPIO2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(SPIRIT1_GPIO2_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : SPIRIT1_GPIO3_Pin */
  GPIO_InitStruct.Pin = SPIRIT1_GPIO3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(SPIRIT1_GPIO3_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : AMPL_CTX_Pin AMPL_BYP_Pin AMPL_CSD_Pin */
  GPIO_InitStruct.Pin = AMPL_CTX_Pin|AMPL_BYP_Pin|AMPL_CSD_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PWM_RF_Pin */
  GPIO_InitStruct.Pin = PWM_RF_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(PWM_RF_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PWR_RF_Pin */
  GPIO_InitStruct.Pin = PWR_RF_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(PWR_RF_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI1_IRQn);

}

/* USER CODE BEGIN 4 */
void HAL_GPIO_EXTI_Callback (uint16_t GPIO_Pin)
{
	int16_t aux_int;
	uint16_t aux_num_pacote;

	if (GPIO_Pin == SPIRIT1_GPIO3_Pin)
	{
		Spirit1GpioIrqGetStatus (&xSpirit1IRQ);
		if (xSpirit1IRQ.IRQ_TX_DATA_SENT == S_SET)
		{
			xTxDoneFlag = SET;
			Spirit1GpioIrqClearStatus();
		}
		if (xSpirit1IRQ.IRQ_MAX_BO_CCA_REACH)
		{
			SpiritSpiCommandStrobes (COMMAND_TX);
			Spirit1GpioIrqClearStatus();
		}
		if (xSpirit1IRQ.IRQ_RX_DATA_DISC)
		{
			SpiritSpiCommandStrobes (COMMAND_RX);
			Spirit1GpioIrqClearStatus();
		}
		if(xSpirit1IRQ.IRQ_RX_DATA_READY)
		{
			xRxDoneFlag = SET;
			cRxData = SpiritLinearFifoReadNumElementsRxFifo();
			SdkEvalSpiReadFifo (cRxData, vectcRxBuff);
			SpiritSpiCommandStrobes (COMMAND_FLUSHRXFIFO);
			spiritstate = RSD_RECEIVED_DATA_READY;
			aux_int = (int16_t)vectcRxBuff[1];
			aux_num_pacote = (aux_int * 256) + (int16_t)vectcRxBuff[0];
			Valor_dBm += SpiritQiGetRssidBm();
			qtd_Valor_dBm++;
			if ((aux_num_pacote < NUM_DE_PACOTES+4) && (aux_num_pacote > 0) && (receber_RF))
			{
				HAL_GPIO_TogglePin (PWM_RF_GPIO_Port, PWM_RF_Pin);
				num_pacote_int = aux_num_pacote;
				time_out_RX = 0;
				pacotes_tx2[num_pacote_int] = 1;
				if (num_pacote_int == 1)
				{// Cabeçalho
					memcpy (&rx2_buffer[0], &vectcRxBuff[2], QTD_BYTES_RF);
					if (TX_curto)
					{
						chegou_pct_curto = 1;
					}
				}
				else
				{// Pontos.
					if ((num_pacote_int == 267) && (!time_espera_pct))
					{
						time_espera_pct = tickseg;
					}
					//Tem que ser [num_pacote-1], os pontos começam do pacote número 2 e o buffer dos pacotes do 1
					memcpy (&rx2_buffer[num_pacote_int-1], &vectcRxBuff[2], QTD_BYTES_RF);
				}
				pacotes_validos++;
			}
			else
			{
				asm("NOP");
			}
			time_out_RX = 0;
			SpiritSpiCommandStrobes (COMMAND_RX);
			Spirit1GpioIrqClearStatus ();
		}
	}
}
void HAL_TIM_PeriodElapsedCallback (TIM_HandleTypeDef *htim)
{
	// esta configurado para "interromper" a cada 1ms (1000 vezes/segundo 1kHz)
	// Este Timer é para decrementar/incrementar as variáveis de tempo
	// Verificar: void HAL_TIM_PeriodElapsedCallback (TIM_HandleTypeDef *htim)
	// Inicia Timer Interrupt
	Time_processo++; // Aqui soma 1ms por vez, tem que estar aqui esta variável
	if (Flag_Led_USB)
	{
		HAL_GPIO_TogglePin (PWM_USB_GPIO_Port, PWM_USB_Pin);
	}
	if (!timer_25ms)
	{// 25 ms
		if (time_out_RF) time_out_RF--;
		if (time_out_RX < 10000) time_out_RX++;
		if (time_ini_USB) time_ini_USB--;
		if (time_espera_pct) time_espera_pct--;
		timer_25ms = 24;
	}
	else
	{
		timer_25ms--;
	}
}
uint16_t tmpInt1;
uint16_t tmpInt2;

void Convert_to_int (float val_float)
{
	float tmpVal = (val_float < 0) ? -val_float : val_float;

	tmpInt1 = tmpVal;                  // Get the integer (678).
	float tmpFrac = tmpVal - tmpInt1;      // Get fraction (0.0123).
	tmpInt2 = tmpFrac * 10;  // Turn into integer (123).
}
void Find_Line_Feed (void)
{
	J = 0;
	for (I = 1; I < 100; I++)
	{
		if (TX_buffer[I] == '\r')
		{
#ifdef COM_LINE_FEED
			J = I + 1;
#else
			J = I;
#endif
			break;
		}
	}
}

uint8_t qtd_f1, qtd_f2, qtd_f3;

void Arquivo_JSON (void)
{
	float V_aux;
	float V_aux4_old;
	uint16_t carga_bat;
	uint16_t tempo_captura;
	uint16_t num_amostras;
	float temperatura;
	float temp_acc;
	int16_t aux_int;
	uint16_t aux_I;
	uint8_t qual_acelerometro;

	if ((rx_buffer[0][0] == 0x1B) && (rx_buffer[0][1] == 0x1B))
	{// Aqui "imprime/envia" se recebeu isso
		//==========================================================
		// abcd efgh ijkl mnop (2 bytes = 16 bits)
		// ab: ( 2 bits para qual eixo )
		// 00 => eixo X => += 0 (0x0000)
		// 01 => eixo Y => += 16384 (0x4000)
		// 10 => eixo Z => += 32768 (0x8000)
		//-----------------------------------
		// c: ( 1 bit para qual sensor )
		// 0 - acelerômetro 16G => += 0 (0x0000)
		// 1 - acelerômetro 50G => += 8192 (0x2000)
		//-----------------------------------
		// d efgh ijkl mnop: ( 13 bits para id-sensor )
		// Nesta configuração eu posso colocar até 2^13 sensores
		// número de sensor variando de 1 até 8191
		//-----------------------------------
		//     1 -  8191 => Eixo X - 16G -> 1 - 8191
		//  8192 - 16383 => Eixo X - 50G -> 1 - 8191
		// 16384 - 24575 => Eixo Y - 16G -> 1 - 8191
		// 24576 - 32767 => Eixo Y - 50G -> 1 - 8191
		// 32768 - 40959 => Eixo Z - 16G -> 1 - 8191
		// 40960 - 49151 => Eixo Z - 50G -> 1 - 8191
		//==========================================================
		aux_I = (uint16_t)rx_buffer[0][3]<<8 | rx_buffer[0][2];
		if (aux_I < 16384)
		{// Eixo X
			qual_eixo = 0;
			if (aux_I < 8192)
			{
				qual_acelerometro = 16;
				id_sensor = aux_I;
			}
			else
			{
				qual_acelerometro = 50;
				id_sensor = aux_I - 8192;
			}
		}
		else
		{
			if (aux_I < 32768)
			{// Eixo Y
				qual_eixo = 1;
				if (aux_I < 24576)
				{
					qual_acelerometro = 16;
					id_sensor = aux_I - 16384;
				}
				else
				{
					qual_acelerometro = 50;
					id_sensor = aux_I - 24576;
				}
			}
			else
			{
				qual_eixo = 2;
				if (aux_I < 40958)
				{
					qual_acelerometro = 16;
					id_sensor = aux_I - 32768;
				}
				else
				{
					qual_acelerometro = 50;
					id_sensor = aux_I - 24576;
				}
			}
		}
		Set_RX_aux.Scale = 1; // 16G
		if (Set_RX_aux.Scale > 3) Set_RX_aux.Scale = 0;
		if ((qual_eixo < 2) || (qual_acelerometro != 50))
		{
			Escala_aux = SCALA_G[Set_RX_aux.Scale];
			Escala_const = 0;
		}
		else
		{
			Escala_aux = 2.7302;
			Escala_const = -50000;
		}
		//======================================
		Transf_Pontos ();
		//======================================
		if (Testar_Transmissao)
		{
			sprintf ((char*)TX_buffer, "{\"pcts\":%4d,\r\n", Pacotes_recebidos);
			HAL_Delay (2);
			CDC_Transmit_FS (TX_buffer, 13);
		}
		else
		{
			sprintf ((char*)TX_buffer, "{\r\n");
			HAL_Delay (2);
			CDC_Transmit_FS (TX_buffer, 1);
		}
		tempo_captura = (uint16_t)rx_buffer[0][5]<<8 | rx_buffer[0][4];
		carga_bat = (uint16_t)rx_buffer[0][7]<<8 | rx_buffer[0][6];
		num_amostras = NUM_MAX_TRANSM_REAL;//(NUM_DE_PACOTES*47);
		temperatura = (float)(((int16_t)(rx_buffer[0][9]<<8)) | rx_buffer[0][8])/10;
		if ((Testar_Transmissao) || (TX_curto))
		{
			temp_acc = (float)(((int16_t)(rx_buffer[0][73]<<8)) | rx_buffer[0][72])/10;
			sprintf ((char*)TX_buffer, "\"data\":\"20%02d-%02d-%02dT%02d:%02d:%02dZ\",\r\n", rx_buffer[0][12],rx_buffer[0][11],rx_buffer[0][10],rx_buffer[0][13],rx_buffer[0][14],rx_buffer[0][15]);
			Find_Line_Feed ();
			HAL_Delay (2);
			CDC_Transmit_FS (TX_buffer, J);
		}
		if (!TX_curto)
		{
			memset (&TX_buffer, 0x00, BYTES_TX+2);
			sprintf ((char*)TX_buffer, "\"janela\":%d,\r\n", tempo_captura);
			Find_Line_Feed ();
			HAL_Delay (2);
			CDC_Transmit_FS (TX_buffer, J);

			memset (&TX_buffer, 0x00, BYTES_TX+2);
			sprintf ((char*)TX_buffer, "\"qtd_poits\":%d,\r\n", num_amostras);
			Find_Line_Feed ();
			HAL_Delay (2);
			CDC_Transmit_FS (TX_buffer, J);
		}
		memset (&TX_buffer, 0x00, BYTES_TX+2);
		aux_int = (temperatura*10);
		V_aux = ((float)(aux_int))/10.0;
		Convert_to_int (V_aux);
		if (V_aux < 0)
		{
			sprintf ((char*)TX_buffer, "\"temp\":-%d.%1d,\r\n", tmpInt1, tmpInt2);
		}
		else
		{
			sprintf ((char*)TX_buffer, "\"temp\":%d.%1d,\r\n", tmpInt1, tmpInt2);
		}
		Find_Line_Feed ();
		HAL_Delay (2);
		CDC_Transmit_FS (TX_buffer, J);

		if ((Testar_Transmissao) || (TX_curto))
		{
			memset (&TX_buffer, 0x00, BYTES_TX+2);
			aux_int = (temp_acc*10);
			V_aux = ((float)(aux_int))/10.0;
			Convert_to_int (V_aux);
			if (V_aux < 0)
			{
				sprintf ((char*)TX_buffer, "\"temp\":-%d.%1d,\r\n", tmpInt1, tmpInt2);
			}
			else
			{
				sprintf ((char*)TX_buffer, "\"temp\":%d.%1d,\r\n", tmpInt1, tmpInt2);
			}
			Find_Line_Feed ();
			HAL_Delay (2);
			CDC_Transmit_FS (TX_buffer, J);
		}
		memset (&TX_buffer, 0x00, BYTES_TX+2);
		sprintf ((char*)TX_buffer, "\"equip_id\":\r\n");
		Find_Line_Feed ();
		HAL_Delay (2);
		CDC_Transmit_FS (TX_buffer, J);
		TX_buffer[0] = '\"';
		J = 0;
		for (I = 0; I < 30; I++)
		{
			if (rx_buffer[0][22+I] != '#')
			{
				TX_buffer[I+1] = rx_buffer[0][22+I];
				J = I + 1;
			}
			else
			{
				break;
			}
		}
		TX_buffer[J+1] = '\"';
		TX_buffer[J+2] = ',';
		HAL_Delay (2);
		CDC_Transmit_FS (TX_buffer, J+3);
		memset (&TX_buffer, 0x00, BYTES_TX+2);
		sprintf ((char*)TX_buffer, "\"bateria\":%4.0f,\r\n", (float)carga_bat);
		Find_Line_Feed ();
		HAL_Delay (2);
		CDC_Transmit_FS (TX_buffer, J);
		if ((Testar_Transmissao) || (TX_curto))
		{
//			Valor_dBm = SpiritQiGetRssidBm();
			if (qtd_Valor_dBm > 0)
			{
				Valor_dBm = (Valor_dBm/qtd_Valor_dBm);
			}
			sprintf ((char*)TX_buffer, "\"dBm\":%5.1f", Valor_dBm);
			TX_buffer[11] = '}';
			TX_buffer[12] = '\r';
			TX_buffer[13] = '\n';
			HAL_Delay (2);
			CDC_Transmit_FS (TX_buffer, 14);
			if (!qual_eixo)
			{
				TX_buffer[0] = '\r';
				TX_buffer[1] = '\n';
				HAL_Delay (2);
				CDC_Transmit_FS (TX_buffer, 2);
			}
			imprime_pacotes = 0;
		}
		else
		{
			memset (&TX_buffer, 0x00, BYTES_TX+2);
			sprintf ((char*)TX_buffer, "\"z_mg\":[\r\n");
			Find_Line_Feed ();
			HAL_Delay (2);
			CDC_Transmit_FS (TX_buffer, 8);
			imprime_pacotes = 1;
		}
	}
	else
	{
		imprime_pacotes = 0;
	}
	if ((imprime_pacotes) && (!TX_curto))
	{
		IT = 0; // para somar número de pontos/coletas
		V_aux4_old = 0;
		IT2 = 0;
		while (IT < NUM_MAX_TRANSM_REAL)//NUM_DE_COLETAS)
		{
			memset (&TX_buffer, 0x00, 110);
			if ((IT + 9) < NUM_DE_COLETAS)
			{
				IT1 = 10;
			}
			else
			{
				IT1 = (NUM_DE_COLETAS - IT);
			}
			qtd_f1 = 0;
			qtd_f3 = 0;
			for (Iusb = 0; Iusb < 10; Iusb++)
			{
				memset (&buffer_usb, 0x00, 12);
				memset (&buffer_usb2, 0x00, 12);
				aux_I = data_acceleration[IT+Iusb];
				if (aux_I != 32765)
				{// Valor válido
					V_aux = (((float)(data_acceleration[IT+Iusb]))*Escala_aux) + Escala_const;
					if ((V_aux < V_minimo) || (V_aux > V_maximo))
					{
						V_aux = V_aux4_old;
					}
					else
					{
						V_aux4_old = V_aux;
					}
					V_aux -= Gravidade_media;
					sprintf ((char*)buffer_usb, "%8.1f,", V_aux);
					qtd_f2 = 0;
					for (I = 0; I < 9; I++)
					{
						if (buffer_usb[I] != 0x20)
						{
							buffer_usb2[qtd_f2] = buffer_usb[I];
							qtd_f2++;
						}
					}
					memcpy (&TX_buffer[qtd_f3], buffer_usb2, qtd_f2);
					qtd_f1 = qtd_f2;
					qtd_f3 += qtd_f1;
				}
				else
				{// Valor inválido
					sprintf ((char*)buffer_usb, "null,");
					memcpy (&TX_buffer[qtd_f3], buffer_usb, 5);
					qtd_f1 = 5;
					qtd_f3 += qtd_f1;
				}
			}
			HAL_Delay (2);
			if (IT1 == 10)
			{
				CDC_Transmit_FS (TX_buffer, qtd_f3);
			}
			else
			{
				// Para não imprimir "," no Final
				CDC_Transmit_FS (TX_buffer, (qtd_f3-1));
			}
			IT += IT1;
		}
		sprintf ((char*)TX_buffer, "]\r\n");
		HAL_Delay (2);
#ifdef TESTE_MARCEL
		CDC_Transmit_FS (TX_buffer, 3);
#else
		CDC_Transmit_FS (TX_buffer, 1);
#endif
		sprintf ((char*)TX_buffer, "}\r\n");
		HAL_Delay (2);
#ifdef TESTE_MARCEL
		CDC_Transmit_FS (TX_buffer, 3);
#else
		CDC_Transmit_FS (TX_buffer, 1);
#endif
	}
}
void recuperar_pct (void)
{
}
void Ajusta_Pacotes (void)
{
}
void Verifica_Pacotes (void)
{
}
void routine_spirit_driver (void)
{
	switch (spiritstate)
	{
	case RSD_TX:
		xTxDoneFlag = RESET;
		SpiritSpiCommandStrobes (COMMAND_FLUSHTXFIFO);
		SdkEvalSpiWriteFifo (QTD_BYTES_RF, vectcTxBuff);
		SpiritSpiCommandStrobes (COMMAND_TX);
		spiritstate = RSD_WAIT_TX_FLAG;
		break;
	case RSD_WAIT_TX_FLAG:
		if (xTxDoneFlag)
		{
			spiritstate = RSD_RX;
		}
		break;
	case RSD_RX:
		SpiritCsma (S_DISABLE);
		spiritstate = RSD_IDLE;
		break;
	case RSD_RECEIVED_DATA_READY: // no thing to do here: wait for superior layer to get msg from the RX buffer
		spiritstate = RSD_RECEIVED_DATA_READY;
		break;
	case RSD_IDLE:
		spiritstate = RSD_IDLE;
		break;
	}
}
void Iniciar_Spirit1 (uint8_t RX_TX, uint32_t Freq_RX_TX)
{
	uint8_t tmpreg;
	uint8_t Config_Spirit1;

	//=============================================================================================
	// Inicia Spirit
	// ** Enable Conversor RF ON
	PWR_RF(GPIO_PIN_SET);
	HAL_Delay(5); // para estabilizar tensão
	//==============================================================================================
	// ** Coloca o CS em 0 para habilitar SPIRIT1
	SPIRIT1_CS(GPIO_PIN_RESET);
	// Inicia SPI_RF
	MX_SPI1_Init ();
	//=============================================================================================
	Config_Spirit1 = 0;
	tttv_conf_RF = 4;
	while (!Config_Spirit1)
	{
		tttv_res_RF = 10;
		SPIRIT1_SDN(GPIO_PIN_SET);
		do
		{
			SpiritSpiReadRegisters (0xC1, 1, &tmpreg); // State
			tmpreg = tmpreg>>1;
			if (tttv_res_RF)
			{// Tentativas para reset
				tttv_res_RF--;
			}
			else
			{// Não resetou Spirit, não dá para transmitir
				Estado_Leitura = EST_DORMIR;
				break;
			}
		}
		while ((tmpreg != 0) && (tmpreg != 0x7F));
		HAL_Delay (20);
		tttv_res_RF = 10;
		SPIRIT1_SDN(GPIO_PIN_RESET);
		HAL_Delay (20);
		do
		{
			SpiritSpiReadRegisters (0xC1, 1, &tmpreg); // State
			tmpreg = tmpreg>>1;
			if (tttv_res_RF)
			{// Tentativas para reset
				tttv_res_RF--;
			}
			else
			{// Não resetou Spirit, não dá para transmitir
				Estado_Leitura = EST_DORMIR;
				break;
			}
		}
		while (tmpreg != MC_STATE_READY);
		if ((tttv_conf_RF) && (Estado_Leitura == EST_DORMIR))
		{// Caso não conseguiu resetar o Spirit1, tenta por mais 3 vezes
			tttv_conf_RF--;
			Estado_Leitura = EST_GATEWAYRX_INIT;
		}
		else
		{// Configura Spirit1 se acima Resetar Spirit1
			tttv_conf_RF--;
			HAL_Delay (10);
			//=============================================================================================
			SpiritExtraCurrent ();
			Spirit1GpioIrqDeInit ();
			SpiritIdentificationRFBoard ();
			/* Initialize the signals to drive the range extender application board */
			SpiritRangeExtInit ();
			Config_Spirit1 = SpiritBaseConfiguration (Freq_RX_TX, BAUD_RATE);
		}
		if (!tttv_conf_RF)
		{// Caso não consiga resetar Spirit1, vai para o estado "Dormir"/Stop_Mode
			Estado_Leitura = EST_DORMIR;
			return;
		}
	}
	// Spirit IRQ config
	// Só configura IRQ se realmente conseguir configurar Spirit1
	SpiritGpioInit (&xGpioIRQ3);
	if (RX_TX)
	{// TX
		Spirit1GpioIrqConfig (TX_DATA_SENT, S_ENABLE);
		Spirit1GpioIrqConfig (RX_DATA_READY, S_ENABLE);
		Spirit1GpioIrqConfig (RX_DATA_DISC, S_ENABLE);
		Spirit1GpioIrqConfig (MAX_RE_TX_REACH, S_ENABLE);
		Spirit1GpioIrqClearStatus ();
		Spirit1PktBasicSetPayloadLength (QTD_BYTES_RF+2);
		SpiritTimerSetRxTimeoutMs (10);
		SpiritSpiCommandStrobes (COMMAND_READY);
		Config_Spirit1 = SpiritVcoCalibration ();
		Spirit1GpioIrqClearStatus ();
		SpiritSpiCommandStrobes (COMMAND_TX);
		// Configura Amplificador como TX
		SKY_CSD(GPIO_PIN_SET);
		SKY_CTX(GPIO_PIN_SET);
		SKY_BYP(GPIO_PIN_SET);
	}
	else
	{// RX
		Spirit1GpioIrqDeInit ();
		Spirit1GpioIrqConfig (TX_DATA_SENT, S_ENABLE);
		Spirit1GpioIrqConfig (RX_DATA_READY, S_ENABLE);
		Spirit1GpioIrqConfig (RX_DATA_DISC, S_ENABLE);
		Spirit1GpioIrqConfig (MAX_RE_TX_REACH, S_ENABLE);
		SpiritSpiCommandStrobes (COMMAND_READY);
		SpiritVcoCalibration ();
		Spirit1PktBasicSetPayloadLength (BYTES_TX);
		SpiritTimerSetRxTimeoutMs (10);
		Spirit1GpioIrqClearStatus ();
		SpiritSpiCommandStrobes (COMMAND_RX);
		spiritstate = RSD_RX;
		Spirit1GpioIrqClearStatus();
		// Configura Amplificador como RX
		SKY_CSD(GPIO_PIN_SET);
		SKY_BYP(GPIO_PIN_RESET);
		SKY_CTX(GPIO_PIN_RESET);
	}
	HAL_NVIC_SetPriority (EXTI1_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ (EXTI1_IRQn);
	Estado_Leitura = EST_GATEWAYRX_READ;
}
void Desliga_Spirit1 (void)
{
	SpiritSpiCommandStrobes (COMMAND_SLEEP);
	SPIRIT1_CS(GPIO_PIN_RESET);
	SPIRIT1_SDN(GPIO_PIN_RESET);
	SKY_CSD(GPIO_PIN_RESET);
	SKY_BYP(GPIO_PIN_RESET);
	SKY_CTX(GPIO_PIN_RESET);
	// Desabilita external interrupt SPIRIT1_GPIO3
//	HAL_NVIC_DisableIRQ (EXTI1_IRQn);
	PWR_RF(GPIO_PIN_RESET);
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
