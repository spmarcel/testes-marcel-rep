Criado em 21/02/2022

Esta pasta foi criada para estudar o comportamento "estranho" do acelerômetro.

Vamos fazer as seguintes alterções:
1) implemetar no sensor a USB para imprimir dados do acelerômetro antes de enviar para o Gateway-RX
2) verificar os 3 eixos para verificar se estão corretos na questão de eixo lido
3) implementar um filtro para eliminar g <= 50mg
